<!DOCTYPE html>
<html>
	<head>
		<!--styles and fonts-->
		<link rel="stylesheet" type="text/css" href="css/wttsg.css" />
		<link rel="stylesheet" type="text/css" href="css/animate.css"> 
		<link rel="stylesheet" type="text/css" href="css/admin.css">  
		<link rel='stylesheet' type='text/css' href='http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300|Noto+Serif|Playball' >
		<!--icon-->
		<link rel="SHORTCUT ICON" href="assets/favicon.ico" type="image/x-icon" />
		<link rel="ICON" href="assets/favicon.ico" type="image/ico" />
		<!--JavaScript-->
		<script type="text/javascript" src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
		<script type="text/javascript" src="js/wttsg.js"></script>
		<!--<script type="text/javascript" src="js/jquery.lettering.js"></script> --> 
		<!-- <script type="text/javascript" src="js/jquery.textillate.js"></script> -->
		<title>
			AguaClara: Water Treatment Technology Selection Guide
		</title>
		<meta charset="utf-8">
		<meta name="google-translate-customization" content="36ccb559a5f1bd35-de9ccffe41c52b89-g781a325f074b1e4b-9"></meta>
	</head>

	
	<?php require_once 'required/config.php';
	require_once 'required/countryarray.php';

	function contaminantsSession(){
		$mysqli = new mysqli(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_NAME);
		$desc_res = $mysqli->query("DESCRIBE `constraints`;");
		$contaminants = [];
		while($row = $desc_res->fetch_row()){
		    $contaminants[] = $row[0];
		}
		// print_r($contaminants);
		$contaminants = array_diff($contaminants, ["treatmentOption", "Turbidity", "Population"]); //remove these, aren't contaminants
		$_SESSION['contaminants'] = array_values($contaminants);
		// print_r($_SESSION['contaminants']);
	
	}

	function microbesSession(){
		 //add to session var a list of microbes in DB!
		$mysqli2 = new mysqli(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_NAME);
		$desc_res2 = $mysqli2->query("DESCRIBE `microbes`;");
		$microbel = [];
		while($row2 = $desc_res2->fetch_row()){
		    $microbel[] = $row2[0];
		}
		$microbel = array_diff($microbel, ["treatmentOption"]); 
		$_SESSION['microbes'] = array_values($microbel);
	}

	function nonboolRemovedSession(){
		$contaminants= $_SESSION["contaminants"];
		$mysqli = new mysqli(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_NAME);
		$rows = $mysqli->query("SELECT * FROM `nonboolean_contaminants`");
		$contents = [];
		while($row = $rows->fetch_row()){
		    $contents[] = $row[0];
		}
		$contents = array_diff($contaminants, $contents);
		$contents = array_values($contents);
		$_SESSION['boolContam'] = $contents;
	}

	function boolRemovedSession(){
		$mysqli = new mysqli(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_NAME);
		$rows = $mysqli->query("SELECT * FROM `nonboolean_contaminants`");
		$contents = [];
		while($row = $rows->fetch_row()){
		    $contents[] = array($row[0],$row[1]);
		}
		$_SESSION['nonboolContam'] = $contents;
	}

	if(!isset($_SESSION['contaminants'])){
		contaminantsSession();
	}

	if(!isset($_SESSION['microbes'])){
		microbesSession();
	}

	if(!isset($_SESSION['boolContam'])){
		nonboolRemovedSession();
	}

	if(!isset($_SESSION['nonboolContam'])){
		boolRemovedSession();
	}
	?>
	
	<body>
		
		<div class="wrapper">
		
				<header class="large"> <!--the nav bar-->
					<div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
  new google.translate.TranslateElement({pageLanguage: 'en'}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>

					<div class="nav1">
						<div class="left"><span><a href="http://aguaclara.cornell.edu">AGUACLARA HOME</a></span></div>
						<div class="right"><span><a href="https://confluence.cornell.edu/display/AGUACLARA/Home">AGUACLARA WIKI</a></span></div>
					</div>
					<div class="nav2">
						<div class="left"><span>TECHNOLOGY GUIDE</span></div>
						<div class="right"><span>AGUACLARA, LLC.</span></div>
					</div>
				</header>
				
				<a href="index.php">
					<div class="circle">
						<div class="mycontent">
							<img src="assets/logo3.png" alt="logo" id="logo">
						</div>
					</div>
				</a>
				
				<div class="padding" id="form">
					<h1>
						<a href="index.php">AguaClara: Water Treatment Technology Selection Guide</a>
					</h1>
				</div>
			
			<div class="wrappedCont">
				<div class="content">