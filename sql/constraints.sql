-- phpMyAdmin SQL Dump
-- version 2.8.0.1
-- http://www.phpmyadmin.net
-- 
-- Host: custsql-ipg99.eigbox.net
-- Generation Time: Mar 24, 2015 at 06:28 PM
-- Server version: 5.5.40
-- PHP Version: 4.4.9
-- 
-- Database: `aguaclara`
-- 

-- --------------------------------------------------------

-- 
-- Table structure for table `constraints`
-- 

CREATE TABLE `constraints` (
  `FimePaom` tinyint(4) NOT NULL,
  `FimeFluoride` tinyint(4) NOT NULL,
  `FimeNitrate` tinyint(4) NOT NULL,
  `FimeUranium` tinyint(4) NOT NULL,
  `FimeArsenic` tinyint(4) NOT NULL,
  `FimeMicrobes` tinyint(4) NOT NULL,
  `FimeRadium` tinyint(4) NOT NULL,
  `AguaClaraPaom` tinyint(4) NOT NULL,
  `AguaClaraFluoride` tinyint(4) NOT NULL,
  `AguaClaraNitrate` tinyint(4) NOT NULL,
  `AguaClaraUranium` tinyint(4) NOT NULL,
  `AguaClaraArsenic` tinyint(4) NOT NULL,
  `AguaClaraMicrobes` tinyint(4) NOT NULL,
  `AguaClaraRadium` tinyint(4) NOT NULL,
  `EStaRSPaom` tinyint(4) NOT NULL,
  `EStaRSFluoride` tinyint(4) NOT NULL,
  `EStaRSNitrate` tinyint(4) NOT NULL,
  `EStaRSUranium` tinyint(4) NOT NULL,
  `EStaRSArsenic` tinyint(4) NOT NULL,
  `EStaRSMicrobes` tinyint(4) NOT NULL,
  `EStaRSRadium` tinyint(4) NOT NULL,
  `BiosandPaom` tinyint(4) NOT NULL,
  `BiosandFluoride` tinyint(4) NOT NULL,
  `BiosandNitrate` tinyint(4) NOT NULL,
  `BiosandUranium` tinyint(4) NOT NULL,
  `BiosandArsenic` tinyint(4) NOT NULL,
  `BiosandMicrobes` tinyint(4) NOT NULL,
  `BiosandRadium` tinyint(4) NOT NULL,
  `IronBioPaom` tinyint(4) NOT NULL,
  `IronBioFluoride` tinyint(4) NOT NULL,
  `IronBioNitrate` tinyint(4) NOT NULL,
  `IronBioUranium` tinyint(4) NOT NULL,
  `IronBioArsenic` tinyint(4) NOT NULL,
  `IronBioMicrobes` tinyint(4) NOT NULL,
  `IronBioRadium` tinyint(4) NOT NULL,
  `KiosksPaom` tinyint(4) NOT NULL,
  `KiosksFluoride` tinyint(4) NOT NULL,
  `KiosksNitrate` tinyint(4) NOT NULL,
  `KiosksUranium` tinyint(4) NOT NULL,
  `KiosksArsenic` tinyint(4) NOT NULL,
  `KiosksMicrobes` tinyint(4) NOT NULL,
  `KiosksRadium` tinyint(4) NOT NULL,
  `FimeTurb` int(4) NOT NULL,
  `AguaClaraTurb` int(4) NOT NULL,
  `EStaRSTurb` int(4) NOT NULL,
  `BiosandTurb` int(4) NOT NULL,
  `IronBioTurb` int(4) NOT NULL,
  `KiosksTurb` int(4) NOT NULL,
  `FimePop` int(10) NOT NULL,
  `AguaClaraPop` int(10) NOT NULL,
  `EStaRSPop` int(10) NOT NULL,
  `BiosandPop` int(10) NOT NULL,
  `IronBioPop` int(10) NOT NULL,
  `KiosksPop` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- 
-- Dumping data for table `constraints`
-- 

