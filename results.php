<?php 
session_start();
if(!isset($_SESSION['uniqueUserId'])){
	$_SESSION['uniqueUserId'] = uniqid($prefix = "");
}
include 'header.php'; 

// Processes the data from the last page on the index.php
if (isset($_POST["distributionType"])){
	$_SESSION["userDistributionType"] = $_POST["distributionType"];
}
// Processes all the other data since the results form may have been resubmitted
// through the interactive review form
if (isset($_POST["distChange"])){
	$_SESSION["userDistributionType"] = $_POST["distChange"];
}if (isset($_POST["popChange"])){
	$var = $_POST["popChange"];
	if ($var > 900) {
		$var*=10; $var -= 7000; $var = ($var-$var%1000);
	} else { $var += 100; }
	$_SESSION["userinputpop"] = $var;
}if (isset($_POST["growthChange"])){
	$_SESSION['userinputpopgrowth'] = $_POST["growthChange"];
}if (isset($_POST["usageChange"])){
	$_SESSION['userinputwaterusage'] = $_POST["usageChange"];
}if (isset($_POST["cityChange"])){
	$_SESSION["userinputcity"]=htmlentities(strip_tags( $_POST['cityChange'] ));
}if (isset($_POST["countryChange"])){
	$_SESSION["userinputcountry"]=htmlentities(strip_tags( $_POST['countryChange'] ));
}if (isset($_POST["sourceChange"])){
	$_SESSION["userWaterSourceType"] = $_POST["sourceChange"];
}if (isset($_POST["avgChange"])){
	$str = $_POST["avgChange"];
	preg_match_all('!\d+!', $str, $matches);
	$var = implode(' ', $matches[0]);
	$_SESSION['userinputavgNTU'] = $var;
}if (isset($_POST["maxChange"])){
	$str = $_POST["maxChange"];
	preg_match_all('!\d+!', $str, $matches);
	$var = implode(' ', $matches[0]);
	$_SESSION['userinputmaxNTU'] = $var;
}if (isset($_POST["pageFilledOutChemical"])){
	// if (isset($_POST["chemical"])){
	// 	$_SESSION['userinputchemical'] = $_POST["chemical"];
	// }else{
	// 	$_SESSION['userinputchemical'] = ["no substances or chemicals"];
	// }
}if (isset($_POST["pageFilledOutMicrobe"])){
	if (isset($_POST["microbe"])){
		$_SESSION['userinputmicrobes'] = $_POST["microbe"];
	}else{
		$_SESSION['userinputmicrobes'] = ["no bacteria, viruses, Cryptosporidium, or Giardia"];
	}
}



//MAKE SURE ALL INPUTS ARE SAFE FROM MALICIOUS USER INPUT
$mysqli = new mysqli(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_NAME);
$nameQ = $mysqli->real_escape_string(strip_tags($_SESSION["userinputname"]));
$orgQ = $mysqli->real_escape_string(strip_tags($_SESSION["userinputorg"]));
$cityQ = $mysqli->real_escape_string(strip_tags($_SESSION["userinputcity"]));
$countryQ = $mysqli->real_escape_string(strip_tags($_SESSION["userinputcountry"]));
$popQ = $mysqli->real_escape_string(strip_tags($_SESSION["userinputpop"]));
$tavgQ= $mysqli->real_escape_string(strip_tags($_SESSION["userinputavgNTU"]));
$tmaxQ= $mysqli->real_escape_string(strip_tags($_SESSION["userinputmaxNTU"]));
$pcapQ= $mysqli->real_escape_string(strip_tags($_SESSION["userinputwaterusage"]));
$pgroQ= $mysqli->real_escape_string(strip_tags($_SESSION["userinputpopgrowth"]));
$distQ= $mysqli->real_escape_string(strip_tags($_SESSION["userDistributionType"]));
$srcQ = $mysqli->real_escape_string(strip_tags($_SESSION["userWaterSourceType"]));

$uniqueUserID = $mysqli->real_escape_string(strip_tags($_SESSION['uniqueUserId']));
//save the current data into MySQL table
$query1 = 'INSERT INTO `wttsg`.`userdata` (`visitorID`, `name`, `organization`, `city`, `country`, `population`, `turbidityAvg`, `turbidityMax`, `uniqueUserID`, `popGrowthRate`, `perCapitaDemand`, `distributionType`, `sourceType`'; 
$query2 = 'VALUES (NULL, "'.$nameQ.'", "'.$orgQ.'", "'.$cityQ.'", "'.$countryQ.'", "'.$popQ.'", "'.$tavgQ.'", "'.$tmaxQ.'", "'.$uniqueUserID.'", "'.$pgroQ.'", "'.$pcapQ.'", "'.$distQ.'",  "'.$srcQ.'"';
//add booleans for contaminants
foreach ($_SESSION['contaminants'] as $ind => $contaminant) {	
	$query1= $query1.", `".$contaminant."`";
	if(in_array($contaminant, $_SESSION["userinputchemical"])){
		$query2= $query2.', "1"';
	}else if(in_array($contaminant, $_SESSION["userinputnonbool"])){
		$query2 = $query2.', "'.$_SESSION['userinputnonbool'][$contaminant].'"';
	}else{$query2= $query2.', "0"';}
} 
//add booleans for microbes
foreach ($_SESSION['microbes'] as $ind => $microbe) {	
	$query1= $query1.", `".$microbe."`";
	if(in_array($microbe, $_SESSION["userinputmicrobes"])){
		$query2= $query2.', "1"';
	}else{$query2= $query2.', "0"';}
} 
// foreach ($_SESSION['userinputnonbool'] as $ind => $contaminant) {	
// 	if($contaminant && $_SESSION['userinputnonbool'][$contaminant]){
// 		$query1= $query1.", `".$contaminant."`";
// 		$query2= $query2.', "'.$_SESSION['userinputnonbool'][$contaminant].'"';
// 	}
// } 
$query = $query1.") ".$query2.");";
echo $query;
$mysqli->query($query);


$usePermalink = true; 

if(isset($_GET["results"])){
	if($_GET["results"] != $_SESSION['uniqueUserId']){
		
		$contaminants = $_SESSION['contaminants'];
		//do things for the permalink
		$permalinkID = $mysqli->real_escape_string(strip_tags($_GET["results"]));
		$permalinkQuery = "SELECT * FROM `userdata` WHERE `uniqueUserID` = '".$permalinkID."';";
		$queryResult = $mysqli->query($permalinkQuery);
		if ($queryResult && $queryResult->num_rows == 1) {
			while ($row = $queryResult->fetch_assoc()) {
				$_SESSION["userinputname"] = $row["name"];
				$_SESSION["userinputorg"] =$row["organization"];
				$_SESSION["userinputcity"] = $row["city"];
				$_SESSION["userinputcountry"] = $row["country"];
				$_SESSION["userinputpop"] = $row["population"];
				$_SESSION['userinputavgNTU'] = $row['turbidityAvg'];
				$_SESSION['userinputmaxNTU'] = $row['turbidityMax'];
				$chemicalList = array();
				$chemicals = $contaminants;
				foreach ($chemicals as $chemical){
					if($row[$chemical]){
						array_push($chemicalList, $chemical);
					}
				}if(count($chemicalList)==0){
					array_push($chemicalList, "no substances or chemicals");
				}
				$_SESSION['userinputchemical'] = $chemicalList;
				$_SESSION['userinputpopgrowth'] = $row['popGrowthRate'];
				$_SESSION['userinputwaterusage'] = $row['perCapitaDemand'];
				$_SESSION["userDistributionType"] = $row['distributionType'];
				$_SESSION["userWaterSourceType"] = $row['sourceType'];
	
		    }
			$_SESSION['uniqueUserId'] = $permalinkID;
			echo"<script>
			jQuery.extend({
			    getValues: function() {
			        var result = null;
			        $.ajax('required/ajaxSession.php',
					{async: false,
						success: function(data) {
			        	//console.log(data);
			        	result = data;
			      	},
			      	error: function() {
			         console.log('An error occurred');
			      	}
					});
			       return result;
			    }
			});
			var results = $.getValues();
			results = JSON.parse(results);
			console.log(results);
			// Reload the current page, without using the cache. Needs so the $results var can contain AJAX results in time
			document.location.reload(true);
			</script>";
		}else{
			echo "<h2 style='color:red;'>Non Valid ID string used.</h2>";
			echo "Error code:".$queryResult->num_rows;
		}
		echo $queryResult;
		
	}
}


$_SESSION['uniqueUserId'] = uniqid($prefix = "");


// echo "ID IS HERE\n";
// echo $_SESSION['uniqueUserId'];

?>

<script type="text/javascript">

document.querySelector("html").style.backgroundImage = "url('assets/background.jpg')";

//get session variables using ajax
jQuery.extend({
    getValues: function() {
        var result = null;
        $.ajax('required/ajaxSession.php',
		{async: false,
			success: function(data) {
        	//console.log(data);
        	result = data;
      	},
      	error: function() {
         console.log('An error occurred');
      	}
		});
       return result;
    }
});

var results = $.getValues();
results = JSON.parse(results); //this variable holds javascript version of session variable
console.log(results);
</script>

<?php
/**
*prints the log and array of possible suggester technologies
*/
function suggest(){
	$mysqli = new mysqli(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_NAME);
	// $desc_res = $mysqli->query("DESCRIBE `constraints`;");
	// $contaminants = [];
	// while($row = $desc_res->fetch_row()){
	//     // $contaminants[$row[0]]= 0;
	// }
	// unset($contaminants["treatmentOption"]);  
	// unset($contaminants["Turbidity"]);  
	// unset($contaminants["Population"]);  
	$mysqli = new mysqli(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_NAME);
	$logicQuery = 'SELECT * FROM constraints';
	$logicResult = $mysqli->query($logicQuery);
	$col_res = $mysqli->query("DESCRIBE constraints");
	$data[] = $col_names;
	while($row = $logicResult->fetch_assoc()){
		$data[] = $row;
	}
	while($row = $col_res->fetch_row()){
	    $col_names[] = $row[0];
	    $types[$row[0]] = $row[1]; 
	}
	$colToSession = array_merge(array("population" => $_SESSION["userinputpop"],
							"turbidity" => $_SESSION["userinputavgNTU"]), $contaminants);
	//print_r($_SESSION);
	if (isset($_SESSION["userinputchemical"])){
		//actually put the session value for each contaminant here
		foreach ($_SESSION["userinputchemical"] as $cont) {
			$colToSession[$cont] = 1;
		}
	}
	$log = "";
	foreach ($types as $col => $type) {
		if ($col != 'treatmentOption'){
			$i = 0;
			foreach ($data as $option) {
				$need = $colToSession[$col];//sadly, they don't match up, use a dict
				$can = $option[$col]; 
				if ($type == 'tinyint(4)'){
					if ($need && !$can) {
						$log = $log."Can't treat ".$option["treatmentOption"]." because of ".$col."\n";
						unset($data[$i]);
					}
				} else if ($type == 'int(4)'){
					if ($need <= $can){
						$log = $log."Can't treat ".$option["treatmentOption"]." because of ".$col."\n";
						unset($data[$i]);
					}
				}
				$i += 1;
			}
		}
	}
	print($log);
	//print_r($data);
	//$suggest[];
	foreach ($data as $ind => $row) {
		$suggest[] = $row["treatmentOption"];
	}
	print_r($suggest);
	//suggest contains an array of the possible treatment types that we would suggest
	//log contains a wordy list of why we didn't suggest the other types
}

/* from fall2014 report
per_capita_water_demand in L/day/person, default to 150
pop_growth_rate, in % default to 1.79 (for us 2013 est)
init_pop, the initial population
plant_adequacy in years, default to 20yrs
flow_rate is the flow rate in L/s optional*/
function design_cost($init_pop,$per_capita_water_demand=150, $pop_growth_rate=1.79,  $flow_rate=0,$plant_adequacy=20){
	$p_n = $init_pop * pow(1+$pop_growth_rate/100,$plant_adequacy) / 100;
	//note flow rate here f = q * P(n) / 86400
	$f = $per_capita_water_demand * $p_n / 86400;
	if ($flow_rate < $f) {$flow_rate = $f;}
	//use SQL data, but for now
	$costs_per_flow = array();
	$costs_per_flow[] = 11777;
	$costs_per_flow[] = 7486;
	$costs_per_flow[] = 7722;
	$costs_per_flow[] = 6867;
	$costs_per_flow[] = 6645;
	$flows = array();
	$flows[] = 16;
	$flows[] = 32;
	$flows[] = 32;
	$flows[] = 35;
	$flows[] = 44;
	list($a,$b) = cost_regression($flows,$costs_per_flow);
	$cost_per_litre_per_second = $a*pow($flow_rate,$b);
	return $cost_per_litre_per_second * $per_capita_water_demand * $p_n / 8640;
}

/*from fall2014 report
assuming one person in the plant at all times
8765.81 hours in a year
hourly_wage: USD/hr default to honduran min wage
plant_adequacy: years, default to 20
E is number of employees present at a time, default to 1*/
function wages_cost($hourly_wage = 2.6, $plant_adequacy = 20, $employees = 1){
	return $hourly_wage * $plant_adequacy * $employees * 8765.81;
}

/*from fall2014 report
per_capita_water_demand in L/day/person, default to 150
d_coagulant dose coagulant in kg/L, default to .00002
d_chlorine dose chlorine in kg/L, default to .001
cost_of_coagulant, in USD/kg, default to alum (1.1) also PACl (.908) is given as a possible default
cost_of_chlorine, in USD/kg, default to 2.98*/
function operating_cost($per_capita_water_demand = 150, $d_coagulant = .00002, 	$d_chlorine = .001, $cost_of_coagulant = 1.1, $cost_of_coagulant = 2.98){
	$c_coagulant = $d_coagulant * $cost_of_coagulant * $per_capita_water_demand * .7 * 30.4;
	//c_chlorine is cost of chlorine/person/month
	$c_chlorine = $d_chlorine * $cost_of_chlorine * $per_capita_water_demand * .7 * 30.4;
	return $c_coagulant + $c_chlorine;
}

$resultsTextQuery = 'SELECT * FROM `option_text` WHERE `treatmentOption`="AguaClara"';
$resultsText = $mysqli->query($resultsTextQuery);
while ($row = $resultsText->fetch_assoc()) {
	$introText = $row['introText'];
}


$pictureQuery = "SELECT * FROM `pictures` WHERE `treatmentOption`='AguaClara'";
$picture = $mysqli->query($pictureQuery);
while ($row = $picture->fetch_assoc()){
	$picURL = $row['url'];
	$altText = $row['altText'];
}


echo '
<h2>Your recommended treatment option is a <span class="dramaticSpan">36 L/S AguaClara plant.</span></h2>
<div class="columns"></div>
	<div class="columns">'.$introText.'</div>
	<div class="columns">Praesent fringilla risus quis tellus consequat aliquet.  Et commodo turpis luctus sit amet. In hac habitasse platea dictumst. Duis accumsan ligula eget sagittis tincidunt. Quisque eu neque velit. Nam ut tortor eu elit iaculis ullamcorper id id dolor. Vestibulum non lorem congue, rutrum diam quis, fringilla massa. Quisque vehicula tincidunt facilisis.</div>
	<div class="columns"><img src="'.$picURL.'" alt="'.$altText.'" class="circularPlantPic"/></div>
<div class="columns"></div>';
			
echo'<p>';
//print_r($_SESSION);
//$fmt = numfmt_create( 'de_DE', NumberFormatter::CURRENCY );
//$usd_amt= design_cost_wrapper();
//$amt = numfmt_format_currency($fmt, $usd_amt, "EUR");
//$amt2 = round($usd_amt,-3);
//echo'The cost of an AguaClara plant for YOU is: '.$amt.' '.$amt2;
echo'</p>';
// echo'
// <a href="index.php?p=9" class="arrowlink">
// 	<img src="assets/arrowleft.png" alt="Back" width=80 height=50 class="arrow" id="left7"/>
// </a>';
?>
	</div>
</div>

<!-- draw the traingle things -->
<div class="wrappedCont" style="margin:auto;margin-top:250px;width:80%;">
	<div class="content" id = "editMenu" style="height:450px;padding-top:0px;">
	<svg id="editBackdrop" width="100%" height="450px" style="margin-top:0px;">
		<rect class="arrowDown" width="100%" height="400px" x="0px" y="0px" style="fill:rgba(255,255,255,.2);opacity:1;" />
		<path class="arrowDown" d="M 200,400 L 250,400 L 225,435 Z" style="fill:rgba(255,255,255,.2);opacity:1;"/>

		<rect class="arrowUp" width="200" height="400px" x="0px" y="0px" style="fill:rgba(255,255,255,.2);opacity:0;" />
		<rect class="arrowUp" width="100%" height="400px" x="250px" y="0px" style="fill:rgba(255,255,255,.2);opacity:0;" />
		<path class="arrowUp" d="M 200,0 L 200,400 L 225,365 L 250,400 L 250,0 Z" style="fill:rgba(255,255,255,.2);opacity:0;"/>
		<text class="arrowDown" style="font-style:italic;fill:white;font-size:1.2em;" x="270" y="430">Click any field to edit.</text>
	</svg>


<?php
	//HERE DOWN = THE SESSIONS VARIABLES ARE DISPLAYED AND MADE EDITABLE WHEN CLICKED
	echo '<h2 style="margin-top:-425px;">Displaying results for a plant';
	if (isset($_SESSION["userinputcity"]) && isset($_SESSION["userinputcountry"])){
		echo ' in<span class="dramaticSpan"> <span id="cityChange" class="change">'.
		$_SESSION["userinputcity"].'</span></span>, <span class="dramaticSpan"><span id="countryChange" class="change">'.
		$_SESSION["userinputcountry"].'</span>;</span>';
	}
	echo "</h2>";

	if (isset($_SESSION['userinputpop'])){
		echo '<h2>serving<span class="dramaticSpan"> <span id="popChange" class="change">'
		.$_SESSION["userinputpop"].'</span></span> people ';
		if (isset($_SESSION['userinputpopgrowth'])){
			echo 'with a population growth rate of <span class="dramaticSpan"><span id="growthChange" class="change">'.
			$_SESSION["userinputpopgrowth"].'</span> percent;</span></h2>';
		}
		else{
			echo ";</h2>";
		}
	}

	if (isset($_SESSION['userinputchemical'])){
		$chemLen = count($_SESSION['userinputchemical']);
		if ($chemLen == 1){
			echo '<h2>whose source water contains <span class ="dramaticSpan"><span id="chemChange" class="change">'
			.$_SESSION["userinputchemical"][0]."</span>;</span></h2>";
		}else if($chemLen ==2){
			echo '<h2>whose source water contains <span class ="dramaticSpan"><span id="chemChange" class="change">'
			.$_SESSION["userinputchemical"][0]." and ".$_SESSION["userinputchemical"][1]."</span>;</span></h2>";
		}else { 
			echo '<h2>whose source water contains <span class ="dramaticSpan"><span id="chemChange" class="change">';
			for ($i =0; $i < $chemLen; $i++){
				if ($i==($chemLen-1)){
					echo " and ".$_SESSION['userinputchemical'][$i]."</span>;</span></h2>";
				}else{
					echo $_SESSION['userinputchemical'][$i].", ";
				}
			}
		}
	}
	if (isset($_SESSION['userinputmicrobes'])){
		$micLen = count($_SESSION['userinputmicrobes']);
		echo '<h2>whose source water contains <span class ="dramaticSpan"><span id="micChange" class="change">';
		if ($micLen == 1){
			echo $_SESSION["userinputmicrobes"][0]."</span>;</span></h2>";
		}else if($micLen ==2){
			echo $_SESSION["userinputmicrobes"][0]." and ".$_SESSION["userinputmicrobes"][1]."</span>;</span></h2>";
		}else { 
			for ($i =0; $i < $micLen; $i++){
				if ($i==($micLen-1)){
					echo " and ".$_SESSION['userinputmicrobes'][$i]."</span>;</span></h2>";
				}else{
					echo $_SESSION['userinputmicrobes'][$i].", ";
				}
			}
		}
		echo "</span></h2>";
	}

	if (isset($_SESSION['userinputavgNTU']) && isset($_SESSION['userinputmaxNTU'])){
		echo '<h2>which treats dirty water from<span class ="dramaticSpan"> <span id="avgChange" class="change">'
		.$_SESSION['userinputavgNTU'].'</span> NTU (average) to <span id="maxChange" class="change">'
		.$_SESSION['userinputmaxNTU'].'</span> NTU (maximum)</span>;</h2>';
	}

	if (isset($_SESSION["userinputwaterusage"])){
		echo '<h2>using <span class ="dramaticSpan"> <span id="usageChange" class="change">'
		.$_SESSION['userinputwaterusage'].'</span></span> liters of water per person per day;</h2>';
	}

	if (isset($_SESSION["userWaterSourceType"])){
		echo '<h2>using<span class="dramaticSpan"> <span id="sourceChange" class="change">'
		.$_SESSION['userWaterSourceType'].'</span></span> as a water source ';
	}else{echo '<h2>';} 
	if (isset($_SESSION["userDistributionType"])){
		$dist = $_SESSION["userDistributionType"];
		if($dist=="YES"){echo 'with a<span class="dramaticSpan"> <span id="distChange" class="change">
			distribution system</span></span> currently in place</h2>';}
		elseif($dist=="NO"){echo 'with<span class="dramaticSpan"> <span id="distChange" class="change">
			no distribution system</span></span> currently in place</h2>';}
		else{echo 'with<span class="dramaticSpan"> no distribution system</span> currently in place, but 
			<span id="distChange" class="change">with plans to create one</span></h2>';}
	} else {echo ";</h2>";}

	echo "<div id='editField'></div>";
	//end editable session variable display section

?>
<script>

//Format: id from clickable results page summary fields is the key
//value is an array of prompt to user, the name of the session, and the case for which kind of editable field it is
var fieldDict = {
	"distChange": ["Is a distribution system in place?", "userDistributionType", 1],
	"popChange": ["What population size <br/>will the plant treat?", "userinputpop", 2],
	"sourceChange": ["From what type of source will the plant's water come?", "userWaterSourceType", 1],
	"chemChange": ["What chemicals and substances are present in the water to be treated?", "userinputchemical", 3],
	"avgChange": ["What is the average turbidity of water that will be treated?", "userinputavgNTU", 5],
	"maxChange": ["What is the maximum turbidity of water that will be treated?", "userinputmaxNTU", 5],
	"cityChange": ["In what city will the<br/> plant be built?", "userinputcity", 0],
	"countryChange": ["In what country will<br/> the plant be built?", "userinputcountry", 4],
	"growthChange": ["What is the anticipated population <br/> growth rate for the town? (%)","userinputpopgrowth", 0],
	"usageChange": ["What is the average per capita <br/>daily water use of your town?", "userinputwaterusage", 0],
	"micChange": ["What microbes are present in the water to be treated?", "userinputmicrobes", 3]
};

//options for checkboxes

// resultArray = results['contaminants']; //get from AJAX of contaminant list currently in DB


//messages and data for radio buttons
var radioButtons = {
	"distChange": [
		["Yes, we currently have a distribution system in place",
			"No, we do not currently have a distribution system in place, but we have the funding to create one",
			"No, we do not currently have a distribution system in place, and we have no plans to create one"],
		["YES", "FUNDS", "NO"]
	],
	"sourceChange": [
		["Groundwater", "Surface Water"], 
		["groundwater", "surface water"]
	]
};

//saves the last thing clicked so that clicking the same thing twice closes the menu while different things 
//in a row would open a different menu
var lastClicked = null;

/* Changes the values for the population range slider, which appears when editing that session variable */
function outputUpdate(val) {
	if (val >= 950){
		val*=10; 
		val -= 8000;
		val = (val-val%1000);
		var v2 = val + 1000;
	} else {
  		var v2 = parseInt(val) + 100;
  	}
  	$("#pop")[0].innerHTML = val + " - " + v2;
}


/* add editable field dropdowns. cases are as follows:
CASES: 
	0, it's an editable text field
	1, it's a radio button list
	2, it's a slider
	3, it's checkboxes
	4, it's a dropdown
*/
function addInput(divName, prompt, case_number){
	var newdiv = document.createElement('div');
	var editHeight = 400;//$("#editMenu").height();
	innerHTMLAccumulator = "<form name='forms' action='results.php?results="+results['uniqueUserId']+"' method='post'>";
    if (case_number==0){ //text field
       	innerHTMLAccumulator += "<h2 style='display:inline-block;font-size:1.3em;'>" + fieldDict[prompt][0] + "</h2>" + 
      " <input type='text' id='editor' name='"+prompt+"' style='padding:.35em;font-size:2em;margin-top:70px;display:inline-block;margin-left:25px;font-family:Open Sans Condensed, sans-serif;'"+
      " value='placeholder'>";

  	} else if (case_number ==1){ //radio button list
  		innerHTMLAccumulator += "<h2 style='display:inline-block;font-size:1.3em;margin-top:55px;'>" + fieldDict[prompt][0] + "</h2>";
  		if(prompt =="distChange"){$("#editMenu").css("height", (editHeight + 215)+"px");} //menu is too small for dist
  		radioString = '';
  		var session = results[fieldDict[prompt][1]]; //current val
  		var text = radioButtons[prompt][0];
  		var values = radioButtons[prompt][1];
  		for(i=0; i<text.length; i++){
  			radioString += '<h2><input type="radio" name="'+prompt+'" value="'+values[i]+'" style="width:2em;height:2em;"';
  			if (values[i]==session){ radioString += "checked"; }
  			radioString +=">&emsp;"+text[i]+"</input></h2>";
  		}
  		innerHTMLAccumulator += radioString;

  	} else if (case_number ==2){ //slider
  		innerHTMLAccumulator += "<h2 style='margin-left:10%;float:left;font-size:1.3em;margin-top:70px;'>" + fieldDict[prompt][0] + "</h2>";
  		innerHTMLAccumulator += '<label class="rangelab" style="float:left;margin-top:65px;margin-left:10px;">50</label>';
		
		sessionvar = results['userinputpop'];
		if (sessionvar > 900) {
			sessionvar += 7000;
			sessionvar /=10;
		} else {
			sessionvar -= 100;
		}
		innerHTMLAccumulator += '<input type=range id=population min=50 value=' + sessionvar + ' max=3200 step=50 name='+prompt+' oninput="outputUpdate(value)" style="margin:0px;margin-top:60px;float:left;width:30%">';
		innerHTMLAccumulator += '<label class="rangelab" style="float:left;margin-top:65px;margin-left:10px;">25000</label>';
		
		if (results["userinputpop"] > 900) {
			inc = results["userinputpop"] - 1000;
			innerHTMLAccumulator += '<div id=pop style="display:inline;margin:20px;float:left;margin-top:75px;">' + inc + ' - ' + results["userinputpop"] + '</div>';
		} else {
			inc = results["userinputpop"] + 100;
			innerHTMLAccumulator += '<div id=pop style="display:inline;margin:20px; float:left;margin-top:75px;">'+ results["userinputpop"] +' - '+ inc + '</div>';
		}
		
		
  	} else if (case_number ==3){ //checkboxes
  		innerHTMLAccumulator += "<h2 style='display:inline-block;font-size:1.3em;margin-top:55px;'>" + fieldDict[prompt][0] + "</h2>";
  		sessionChecked = results[fieldDict[prompt][1]];
  		if(prompt == "chemChange"){
  			var resultArray = results['boolContam'];
  			var saveArray = "chemical[]";
  			var pageFilledOut = "pageFilledOutChemical";
  			var addNonBool = true;
  		}else if (prompt=="micChange"){
  			var resultArray = results['microbes'];
  			var saveArray = "microbe[]";
  			var pageFilledOut = "pageFilledOutMicrobe";
  			var addNonBool = false;
  		}
  		tableString = "<input type='hidden' name='"+pageFilledOut+"' value='True'><div style='margin-top:10px;'><table>";
  		var newEditHeight = editHeight+ 50*Math.round(resultArray.length/2.0)+ 150+ "px";
  		$("#editMenu").css("height", newEditHeight);
  		
  		for (var i=0; i<resultArray.length; i++){
  			valueID = resultArray[i];
  			if(i%2==0){tableString+="<tr>";}
  			tableString+='<td class="check"><div class="squaredTwo"><input type="checkbox" name="'+saveArray+'" value="'+valueID+
  			'" id="'+valueID+'"';
  			if($.inArray(valueID, sessionChecked)!=-1){tableString+=' checked';} 
			tableString+='><label for="'+valueID+'"></label></div></td><td><div class="pl">'+valueID+'</div></td>';
  			if(i%2==1){tableString+="</tr>";}
  		}
		tableString+="</table></div>";

		//ACTUALLY PROBABLY PUT THIS ON DIFFERENT PAGE
  		innerHTMLAccumulator += tableString; 
  		if (addNonBool){
  			//Make the page longer or else we'll run out of room
  			var nonBoolAdditionalHeight = 300;
  			$("#editMenu").css("height", newEditHeight + nonBoolAdditionalHeight);
  			results['nonbool']; 
  		}

  	} else if (case_number ==4){ //dropdown
  		innerHTMLAccumulator += "<h2 style='display:inline-block;font-size:1.3em;margin-top:55px;'>" + fieldDict[prompt][0] + "</h2>";
		value = results["userinputcountry"];
		selectString= '<select id="countries" name="'+prompt+'" class="countrydrop" style="margin-left:25px;margin-top:70px;">';
		countryArray = results["countryArray"];
		for(var i=0; i<countryArray.length; i++){
			d = countryArray[i];
			if(value==d){
				selectString += '<option value='+ value + ' selected="selected">' + value + '</option>';
			} else {
				selectString += '<option value="'+ String(d) + '">' + String(d) + '</option>';
			}	
		}
		selectString += '</select>';
		innerHTMLAccumulator += selectString;

  	} else if (case_number == 5){ //photos of turbidity pictures
  		pics = [30, 60, 100, 200, 500];
  		$("#editMenu").css("height", editHeight+315+"px");
  		innerHTMLAccumulator += "<h2 style='display:inline-block;font-size:1.3em;margin-top:55px;'>" + fieldDict[prompt][0] + "</h2>";
  		var accString = "<div style='padding-top:5px;'>"; //will accumulate the contents of the form so options are not updated at different times

  		turb = results[fieldDict[prompt][1]]; //selected turb from sessions

		accString += '<label for="NTU0"><div class="polaroid" data-ntus=0 id="polaroid20" ';
		//allows for showing which was chosen the last time the user entered the data
		if(turb == 0){accString+= 'style="box-shadow: rgb(79,90,94) 1px 1px 12px 5px;"';}

		accString+='><div class="polaroidp">15 NTU</div><div class="verticalhelper"><img src="assets/15ntu.jpg" alt="15 NTU">'
			+'</div></div></label><input type="checkbox" class="hiddenCheckbox" id="NTU0" name="'+prompt+'" value="0"/>';
		
		for(var i =0; i<pics.length; i++){
			var ntuCount = pics[i];
			accString+= '<label for="NTU'+ntuCount+'"><div class="polaroidright" data-ntus='
			+ntuCount+' id="polaroidright'+ntuCount+'" ';
			//allows for showing which was chosen the last time the user entered the data
			if(turb==ntuCount){accString+= 'style="box-shadow: rgb(79,90,94) 1px 1px 12px 5px;"';}
			accString+='><div class="polaroidp">'+ntuCount+' NTU</div><div class="verticalhelper"><img src="assets/'
				+ntuCount+'ntu.jpg" alt="'+ntuCount+' NTUs"></div></div></label><input type="checkbox" class="hiddenCheckbox" id="NTU'
				+ntuCount+'" name="'+prompt+'" value="'+ntuCount+'"/>';
		}

		accString+="<div>";
		innerHTMLAccumulator += accString;
	
  	}
  	newdiv.innerHTML += innerHTMLAccumulator + "<div style='clear:both;display:block,height:30px;width:100%;margin-top:0px;padding-top:7px;padding-bottom:7px;text-align:right;'>"
	+"<input type='submit' id='savePageUpdate' class='savePageUpdate' name='savePageUpdate' value='Save and Refresh'"+
	" style='padding-right:15px;font-size:1.4em;background-color:Transparent;border:none;cursor:pointer;color:white;font-family: \'Open Sans Condensed\', sans-serif;'></input></div></form>";
    document.getElementById(divName).appendChild(newdiv);
	/*log ntu data from turbidity picture buttons*/
	polaroids(); //else clickable drop shadows break
	sneaky('savePageUpdate'); //maintain scroll position

}

/* minimize the dropdown */
function removeInput(divName){
	var element = document.getElementById(divName);
	element.removeChild(element.firstChild);
}


/* if the dropdown isn't visible, make it visible; if we clicked on the same field twice in
	a row, hide it on the second click; if we click different ids each time just change the 
	text and content as appropriate without minimizing anything */
$(".change").click(function() {
  	if(lastClicked!=$(this).attr('id')){
	  	if(lastClicked!=null && $(".arrowDown").css("opacity") == 0){
	  		removeInput("editField");
	  	}
		$(".arrowDown").css("opacity",0);
	  	$(".arrowUp").css("opacity",1);
	  	$("#editMenu").css("height", "575");
	  	addInput("editField", $(this).attr('id'), fieldDict[$(this).attr('id')][2]);
	  	$("#editor").val(results[fieldDict[$(this).attr('id')][1]]); //set default value to session var

  	} else{
	  	if($(".arrowDown").css("opacity") == 1){
		  	$(".arrowDown").css("opacity",0);
		  	$(".arrowUp").css("opacity",1);
		  	$("#editMenu").css("height", "575px");
		  	addInput("editField", $(this).attr('id'), fieldDict[$(this).attr('id')][2]); //add the textbox dropdown
		  	$("#editor").val(results[fieldDict[$(this).attr('id')][1]]); //set default value to session var
	  	}else{
		  	$(".arrowDown").css("opacity",1);
		  	$(".arrowUp").css("opacity",0);
		  	$("#editMenu").css("height", "450px");
		  	removeInput("editField");
	  }
  }
  lastClicked = $(this).attr('id');
  	
});

</script>

</div>
</div>

<div class="wrappedCont" style="margin-top:300px;">
	<div class="content" style="height:430px;">
		<h2>With a flowrate of <span class="dramaticSpan">36 L/s</span>, you can serve your community for <span class="dramaticSpan">15 years.</span></h2>
		<div class="explanatoryText">This projected flowrate is based on calculations involving your 
			community size and a projected daily usage of 50 L/day. Several treatment options that could 
			be effective for your community are enumerated below.</div>
		<div class="fourColumnLayout">
			<div class="fourColumns">
				<div class="boxShapedText">FIME</div>
				Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur interdum tellus orci, Curabitur eu est eu purus lobortis tempor eu nec lectus. Quisque vitae lacus porttitor, interdum nisi efficitur, convallis libero. Nam eget condimentum magna, consequat ornare quam. Vivamus sit amet nisi condimentum, vulputate eros ullamcorper, hendrerit arcu. 
			</div>
			<div class="fourColumns">
				<div class="boxShapedText">AguaClara</div>
				Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur interdum tellus orci, Curabitur eu est eu purus lobortis tempor eu nec lectus. Quisque vitae lacus porttitor, interdum nisi efficitur, convallis libero. Nam eget condimentum magna, consequat ornare quam. Vivamus sit amet nisi condimentum, vulputate eros ullamcorper, hendrerit arcu. 
			</div>
			<div class="fourColumns">
				<div class="boxShapedText">EStaRS</div>
				Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur interdum tellus orci, Curabitur eu est eu purus lobortis tempor eu nec lectus. Quisque vitae lacus porttitor, interdum nisi efficitur, convallis libero. Nam eget condimentum magna, consequat ornare quam. Vivamus sit amet nisi condimentum, vulputate eros ullamcorper, hendrerit arcu. 
			</div>
			<div class="fourColumns">
				<div class="boxShapedText">Biosand Filter</div>
				Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur interdum tellus orci, Curabitur eu est eu purus lobortis tempor eu nec lectus. Quisque vitae lacus porttitor, interdum nisi efficitur, convallis libero. Nam eget condimentum magna, consequat ornare quam. Vivamus sit amet nisi condimentum, vulputate eros ullamcorper, hendrerit arcu. 
			</div>
		</div>

	</div>
</div>

<div class="wrappedCont" style="margin-top:300px;">
	<div class="content" style="height:430px;">
		<h2>Deciding factors: <span class="dramaticSpan">Your plant's constraints</span></h2>
		<div class="explanatoryText">We've identified the factors that can make or break the design of your plant.</div>
		<div class="threeRows"></div>
		<div class="threeRows"></div>
		<div class="threeRows"></div>
	</div>
</div>

<div class="wrappedCont" style="margin-top:350px;">
	<div class="content" style="height:440px;">
		<h2>Find the water treatment option that <span class="dramaticSpan">fits your budget.</span></h2>
		<div class="explanatoryText">Below are cost estimations for various water treatment technologies. Considered factors include
			cost of design, operator wages, and cost of operation/maintenance.
		</div>
		<div class="fourColumnLayout">
			<div class="fourColumns">
				<div class="boxShapedText">$433,303<span class="littleBox">Cost of an AguaClara plant</span></div>
				Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur interdum tellus orci, Curabitur eu est eu purus lobortis tempor eu nec lectus. Quisque vitae lacus porttitor, interdum nisi efficitur, convallis libero. Nam eget condimentum magna, consequat ornare quam. Vivamus sit amet nisi condimentum, vulputate eros ullamcorper, hendrerit arcu. 
			</div>
			<div class="fourColumns">
				<div class="boxShapedText">$433,303<span class="littleBox">Cost of a FIME plant</span></div>
				Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur interdum tellus orci, Curabitur eu est eu purus lobortis tempor eu nec lectus. Quisque vitae lacus porttitor, interdum nisi efficitur, convallis libero. Nam eget condimentum magna, consequat ornare quam. Vivamus sit amet nisi condimentum, vulputate eros ullamcorper, hendrerit arcu. 
			</div>
			<div class="fourColumns">
				<div class="boxShapedText">$433,303<span class="littleBox">Cost of an EStaRS plant</span></div>
				Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur interdum tellus orci, Curabitur eu est eu purus lobortis tempor eu nec lectus. Quisque vitae lacus porttitor, interdum nisi efficitur, convallis libero. Nam eget condimentum magna, consequat ornare quam. Vivamus sit amet nisi condimentum, vulputate eros ullamcorper, hendrerit arcu. 
			</div>
			<div class="fourColumns">
				<div class="boxShapedText">$433,303<span class="littleBox">Cost of a Biosand Filter</span></div>
				Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur interdum tellus orci, Curabitur eu est eu purus lobortis tempor eu nec lectus. Quisque vitae lacus porttitor, interdum nisi efficitur, convallis libero. Nam eget condimentum magna, consequat ornare quam. Vivamus sit amet nisi condimentum, vulputate eros ullamcorper, hendrerit arcu. 
			</div>
		</div>
	</div>
</div>

<div class="wrappedCont" style="margin-top:250px;height:340px;">
	<div class="content" style="height:340px;">
		<h2 style="font-size:2em;padding-bottom:15px;">Interested in making this plant a reality? <b>View next steps here.</b></h2>
		<p class="explanatoryText">Share your treatment option recommendation with others via link or email. </p>
		<input type="text" style="padding:.35em;font-size:2.5em;vertical-align:middle;" value = "agua.clara/yourplanthere" readonly  onfocus="this.select();" onmouseup="return false;"/>
		<div style="display:inline-block;">
		<!--<img src="assets/facebook.png" alt="facebook" height="70px" width="70px" style="vertical-align:middle;display:inline-block;padding:15px;"/>
		<img src="assets/twitter.png" alt="twitter" height="70px" width="70px" style="vertical-align:middle;display:inline-block;padding:15px;"/>
		--><img src="assets/email.png" alt="email" height="70px" width="70px" style="vertical-align:middle;display:inline-block;padding:15px;"/>
	</div>
	<div style="clear:both"></div>
		<!--<div class="squaredTwo" style="display:inline-block;vertical-align:middle;"><input type="checkbox" name="emailList" value="emailList" id="emailList"><label for="emailList"></label></div><div style="display:inline-block;vertical-align:middle;font-size:1.4em;">&emsp;Sign me up for email updates about AguaClara.</div>-->

<script>
$("#emailList").click(function(){
	console.log("helloworld");
}


)

</script>

<?php
include 'footer.php';
?>

