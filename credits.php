<?php 
session_start();
include 'header.php'; 
?>

<h2><span class="dramaticSpan">Credits</span></h2>

<div class="pl"> 
Site designed by Sarah Sinclair '16. <br/>
Recommendations and estimates developed by Anthony Arce '18, Larissa Sakiyama '18 and Yao Lu '15. <br/>
Site implementation by Andrew Mullen '16 and Sarah Sinclair. <br/>
<a href="http://thenounproject.com/term/escalation/24516/">Escalation</a> designed by <a href="http://thenounproject.com/attilio.baghino/">Attilio Baghino</a> from the <a href="http://www.thenounproject.com">Noun Project</a></br>
<a href="http://thenounproject.com/term/waterfall/3411/">Waterfall</a>designed by <a href="http://thenounproject.com/Luis/">Luis Prado</a> from the <a href="http://www.thenounproject.com">Noun Project</a></div>
<?php
include 'footer.php';
?>
