// wttsg.js
// by Sarah Sinclair sjs334@cornell.edu
// 29 October 2014

/*!
 * Scroll Sneak
 * http://mrcoles.com/scroll-sneak/
 *
 * Copyright 2010, Peter Coles
 * Licensed under the MIT licenses.
 * http://mrcoles.com/media/mit-license.txt
 *
 * Date: Mon Mar 8 10:00:00 2010 -0500
 */
var ScrollSneak = function(prefix, wait) {
    // clean up arguments (allows prefix to be optional - a bit of overkill)
    if (typeof(wait) == 'undefined' && prefix === true) prefix = null, wait = true;
    prefix = (typeof(prefix) == 'string' ? prefix : window.location.host).split('_').join('');
    var pre_name;

    // scroll function, if window.name matches, then scroll to that position and clean up window.name
    this.scroll = function() {
        if (window.name.search('^'+prefix+'_(\\d+)_(\\d+)_') == 0) {
            var name = window.name.split('_');
            window.scrollTo(name[1], name[2]);
            window.name = name.slice(3).join('_');
        }
    }
    // if not wait, scroll immediately
    if (!wait) this.scroll();

    this.sneak = function() {
	// prevent multiple clicks from getting stored on window.name
	if (typeof(pre_name) == 'undefined') pre_name = window.name;

	// get the scroll positions
        var top = 0, left = 0;
        if (typeof(window.pageYOffset) == 'number') { // netscape
            top = window.pageYOffset, left = window.pageXOffset;
        } else if (document.body && (document.body.scrollLeft || document.body.scrollTop)) { // dom
            top = document.body.scrollTop, left = document.body.scrollLeft;
        } else if (document.documentElement && (document.documentElement.scrollLeft || document.documentElement.scrollTop)) { // ie6
            top = document.documentElement.scrollTop, left = document.documentElement.scrollLeft;
        }
	// store the scroll
        if (top || left) window.name = prefix + '_' + left + '_' + top + '_' + pre_name;
        return true;
    }
}


$(document).ready(function(){
	//Code I'm not using that makes the scrollbar smaller
	//"body".onload = onLoad();
	/*window.onscroll= function() {
	  if ($(document).scrollTop() > 80) {
		$("header").removeClass("large").addClass("small");
	  } else {
		$("header").removeClass("small").addClass("large");
	  }
	};*/
	
	$("#org").keyup( function() {
		$(".arrowMessage").text("Click to Continue");
		/*$('.arrowMessage').textillate({  
				in: {  effect: 'rollIn' }  
		});  */
	});
	

	/* SCROLL SNEAK: CODE FROM:
	 * http://www.pre-sales.ch/apphp/index.php?page=pages&pid=71
	 * Page will stay loaded at same height when arrows are clicked
	 */
	 
	var arrows = document.body.getElementsByClassName('arrow');
	var ids = [];
	for (var i=0; i<arrows.length; i++){
	   var tag = arrows[i];
	   if (tag.id) { ids.push(tag.id); }
	}
	(function(){
		var a = '';
		for (var j=0; j < ids.length; j++){
		   a = j+'';
		   //a = new ScrollSneak(ids[j]);
		   a = new ScrollSneak(location.hostname);
		   //console.log(ids[j]);
		   var tabs = $(document.getElementById(ids[j])).parent(); // the "a" tags
		   for (var i=0; i < tabs.length; i++) { /*console.log(tabs[i]);*/ tabs[i].onclick = a.sneak; }
		 }
	}
	)();
	//end scroll sneak

	
	/*	(function(){
		var a = '';
		for (var j=0; j < ids.length; j++){
		   a = j+'';
		   a = new ScrollSneak(ids[j]);
		   var newstring = ids[j].substring(0,ids[j].length-1);
		   var newnum = parseInt(ids[j].substring(ids[j].length-1,ids[j].length))+1;
		   var idatj = newstring + newnum.toString();
		   var tabs = $(document.getElementById(idatj)).parent(); // the "a" tags
		   console.log(tabs);
		   console.log("those were the tabs");
		   for (var i=0; i < tabs.length; i++) { tabs[i].onclick = a.sneak; }
		 }
	}
	)(); */
	
	
	/* Some of parallax code from http://bit.ly/1p2yphE */
	$(window).scroll(function () {
		setBackgroundPosition();
	})
	$(window).resize(function() {
		setBackgroundPosition();
	});
	function setBackgroundPosition(){
		$("html").css('background-position', "-" + 0 + "px " + -(Math.max(document.body.scrollTop, document.documentElement.scrollTop) / 3) + "px");
	}
	/*end jquery scroll code*/
});	

/*log ntu data from turbidity picture buttons*/
function polaroids(){
	$(".polaroidright").click(function (){
		console.log(parseInt(this.dataset.ntus));
		$(".polaroidright").css("box-shadow", "1px 1px 5px gray"); 
		$(".polaroid").css("box-shadow", "1px 1px 5px gray");
		$(this).css("box-shadow", "1px 1px 12px 5px #4F5A5E");
	})

	$(".polaroid").click(function (){
		console.log(parseInt(this.dataset.ntus));
		$(".polaroidright").css("box-shadow", "1px 1px 5px gray"); 
		$(".polaroid").css("box-shadow", "1px 1px 5px gray"); 
		$(this).css("box-shadow", "1px 1px 12px 5px #4F5A5E");
	})

	$(".polaroidright2").click(function (){
		console.log(parseInt(this.dataset.ntus));
		$(".polaroidright2").css("box-shadow", "1px 1px 5px gray"); 
		$(".polaroid2").css("box-shadow", "1px 1px 5px gray");
		$(this).css("box-shadow", "1px 1px 12px 5px #4F5A5E");
	})

	$(".polaroid2").click(function (){
		console.log(parseInt(this.dataset.ntus));
		$(".polaroidright2").css("box-shadow", "1px 1px 5px gray");  
		$(this).css("box-shadow", "1px 1px 12px 5px #4F5A5E");
	})

	$("#right0").click(function (){
		console.log($("input[name=user]"));
		x = $("input[name=user]").val();
		console.log(x);
		
	});
}

polaroids();


//save page update scroll sneak must be called WHEN IT EXISTS ON THE PAGE
function sneaky(classID){
	console.log("Sneaking!");
	var save = document.body.getElementsByClassName(classID);
	console.log(save);
	var ids = [];
	for (var i=0; i<save.length; i++){
	   var tag = save[i];
	   if (tag.id) { ids.push(tag.id); }
	}console.log(ids);
	(function(){
		var a = '';
		for (var j=0; j < ids.length; j++){
		   a = j+'';
		   //a = new ScrollSneak(ids[j]);
		   a = new ScrollSneak(location.hostname);
		   //console.log(ids[j]);
		   var tabs = $(document.getElementById(ids[j])).parent(); // the "a" tags
		   for (var i=0; i < tabs.length; i++) { console.log(tabs[i]); tabs[i].onclick = a.sneak; }
		}
	}
	)();
}



function scroll_lock(className){
	var save = document.body.getElementsByClassName(className);
	//console.log(save);
	var ids = [];
	for (var i=0; i<save.length; i++){
	   var tag = save[i];
	   if (tag.id) { ids.push(tag.id); }
	}//console.log(ids);
	(function(){
		var a = '';
		for (var j=0; j < ids.length; j++){
		   a = j+'';
		   //a = new ScrollSneak(ids[j]);
		   a = new ScrollSneak(location.hostname);
		   //console.log(ids[j]);
		   var tabs = $(document.getElementById(ids[j])).parent(); // the "a" tags
		   for (var i=0; i < tabs.length; i++) { /*console.log(tabs[i]);*/ tabs[i].onclick = a.sneak; }
		}
	}
	)();
}

//STUFF NO ONE NEEDS BUT THAT IS HERE FOR FUTURE REFERENCE
/*function onLoad() {
	//source: http://parascroll.wwwtyro.net/
	window.onscroll = function() {
    var speed = 8.0;
    document.body.style.backgroundPosition = (-window.pageXOffset / speed) + "px " + (-window.pageYOffset / speed) + "px";
    }
}*/

/*
//sarah's janky menu bar shrinker
$(window).scroll(function() {
  if ($(document).scrollTop() > 50) {
	$("header").css("height", 40); 
	$("header").css("position", "fixed");
  } else {
	$("header").css("height", 80);
	$("header").css("position", "static");
  }
});
*/