<?php 
	session_start();
	include 'header.php';



	/**
	*writes the table userdata to a file, userdata.???
	*/
	function write_userdata_to_file(){
		$mysqli = new mysqli(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_NAME);
		$desc_res = $mysqli->query("DESCRIBE userdata");
		$fp = fopen('userdata.csv', 'w') or die("Unable to open file!");
		while($row = $desc_res->fetch_row()){
		    $col_names[] = $row[0];
		}
		$data[] = $col_names;


		$query = "SELECT * FROM userdata LIMIT 10000";//artificial limit
		$result = $mysqli->query($query);

		while ($row = $result->fetch_row()){
			$data[] = $row;
		}	

		foreach ($data as $row) {
		    fputcsv($fp, $row);
		}

		fclose($fp);
		return 1;
	}

	//http://stackoverflow.com/questions/4356289/php-random-string-generator
	function generateRandomString($length = 10) {
	    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    $charactersLength = strlen($characters);
	    $randomString = '';
	    for ($i = 0; $i < $length; $i++) {
	        $randomString .= $characters[rand(0, $charactersLength - 1)];
	    }
	    return $randomString;
	}

	//http://stackoverflow.com/questions/2768885/how-can-i-calculate-a-trend-line-in-php
	//returns (a,b) s.t cost/flow = a*flow^b
	function cost_regression($flows,$costs_per_flow){
		// Now convert to log-scale for flows
		$log_flows = array_map('log', $flows);
		$log_cost = array_map('log',$costs_per_flow);
	  	//not just run a linear regression and it should work.
		// Now estimate $a and $b using equations from Math World
		$n = count($flows);
		$square = create_function('$x', 'return pow($x,2);');
		$flows_squared = array_sum(array_map($square, $log_flows));
		$xy = array_sum(array_map(create_function('$x,$y', 'return $x*$y;'), $log_flows, $log_cost));
		$bFit = ($n * $xy - array_sum($log_cost) * array_sum($log_flows)) / ($n * $flows_squared - pow(array_sum($log_flows), 2));

		$aFit = (array_sum($log_cost) - $bFit * array_sum($log_flows)) / $n;
		$aFit = pow(M_E,$aFit);
		//echo '<h1>C = '.$aFit.' * f ^ '.$bFit .'</h1>';
		return array($aFit,$bFit);
	}

	/**
	*table is the table from which you pull
	*target is the target of the form
	*/
	function table_to_form($table, $target) {
		$mysqli = new mysqli(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_NAME);
		echo '<form action='.$target.' method="post">';
		$query = "SELECT * FROM ".$table." LIMIT 1000";
		$result = $mysqli->query($query);
	
		//finding which column has the uid/primary (can't be changed)
		$query = "SELECT COLUMN_NAME FROM KEY_COLUMN_USAGE WHERE TABLE_NAME = '".$table."'";
		$super_mysqli = new mysqli(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_DESC_NAME);
		$uid_res = $super_mysqli->query($query);
		$arr = $uid_res->fetch_assoc();
		$uid_str = $arr["COLUMN_NAME"];
		$uid_name = $uid_str;
		$uid_ind = 0;
		
		$desc_res = $mysqli->query("DESCRIBE ".$table);
		$desc_res2 = $desc_res;
		echo'<table id="admin_table" class="'.$table.'" width="100%">';
		echo'<tr>';
		$desc;
		echo '<td id="admin_col_header"><b>'.$uid_name.'</b></td>';
		$desc[] = $uid_name;
		$col_names[] = $uid_name;
		while($row = $desc_res->fetch_row()){//loops thru all the column names
			if ($uid_str != ""){//finding the index of the column that is the uid
				if ($uid_str == $row[0]){
					$uid_str = "";
				} else {
					$uid_ind ++;
				}
			}
			if ($row[0] != $uid_name){
				echo '<td id="admin_col_header"><b>';
				printf($row[0]);
				$desc[] = $row[0];
				echo '</b></td>';
				$col_names[] = $row[0];
			}
		}
		echo'<td></td></tr>';

		while ($row = $result->fetch_row()){
			//$row[0]
		 	echo'<tr>';
		 	//put the uid first
		 	$uid_val = $row[$uid_ind];
		 	echo '<td>'.$uid_val.'</td>';//note uid cannot be changed

		 	for ($i = 1; $i < count($row); $i++){
		 		//DATA
		 		// if ($i != $uid_ind){
		 			echo '<td>';
		 			
		 			if(in_array($desc[$i], $_SESSION['contaminants']) || in_array($desc[$i], $_SESSION['microbes']) ){
						echo '<input type="text" size="3" name="'.$row[$uid_ind].'_-_'.$desc[$i].'_--_'.$uid_name.'" value='.$row[$i].'>';
					}else if($table == "option_text"){
						echo '<textarea rows="5" cols="20" name="'.$row[$uid_ind].'_-_'.$desc[$i].'_--_'.$uid_name.'" style="resize:none;font-size: 8pt;">'.$row[$i].'</textarea>';
					}else if($table =="regression"){
						echo '<input type="text" size="16" name="'.$row[$uid_ind].'_-_'.$desc[$i].'_--_'.$uid_name.'" value='.$row[$i].'>';
					}else{
						echo '<input type="text" size="16" name="'.$row[$uid_ind].'_-_'.$desc[$i].'_--_'.$uid_name.'" value='.$row[$i].'>';
					}
		 			echo ' </td> ';
		 		// }
		 	}
		 	echo'<td><input type="submit" value="delete" name="delete_-_'.$uid_name.'_--_'.$uid_val.'" class="deleteButton"></td>';
		 	echo'</tr>';
		}
		echo '<tr id="row_for_spacing"><td><p> </p></td></tr><tr>';

		foreach ($col_names as $col) {
			if(in_array($col, $_SESSION['contaminants']) || in_array($col, $_SESSION['microbes']) ){
				echo '<td><b> </b> <br/><input type="text" size ="3" name="addrow-'.$col.'" placeholder='.$col.'>';
			}else if($table == "option_text" && $col!="treatmentOption"){
				echo '<td><textarea rows="5" cols="20" name="addrow-'.$col.'" placeholder='.$col.' style="resize: none;font-size:8pt;" ></textarea>';
			}else{
				echo "<td>";
				if ($col=="treatmentOption"){echo "<b>Add New Treatment Option</b><br/>";}
				else if($col=="PlantName"){echo "<b>Add Data for Another Plant</b><br/>";}
				else{echo "<b> </b> <br/>";}
				echo '<input type="text" size="16" name="addrow-'.$col.'" placeholder='.$col.'>';
			}
			echo '</td>';
		}
		echo '</tr>';
		echo'</table>';
		echo'<input type="submit" name="submit" value="submit" class="loginButton">
			</form>';
		return 0;
	}

	function nonbool_const_form($table, $target) {
		$mysqli = new mysqli(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_NAME);
		$rows = $mysqli->query("SELECT * FROM `".$table."`");
		$contents = [];
		while($row = $rows->fetch_row()){
		    $contents[$row[0]] = $row[1];
		}
		// echo var_dump($contents);
		echo '<form action='.$target.' method="post">';
		echo '<table id="admin_table" class="'.$table.'" width="80%"><tr><td style="width:100px;"><b style="width:100px;">Contaminants</b></td>'; 
		foreach ($_SESSION['contaminants'] as $key => $value) {
			echo "<td><b>".$value."</b></td>";
			}
		echo "</tr><tr><td style='width:100px;'><p style='width:100px;'>Needs Non Bool Data?</p></td>";
		$tableString ="";

		foreach ($_SESSION['contaminants'] as $key => $value) {
			$tableString=$tableString.'<td class="check"><div class="squaredTwo"><input type="checkbox" name="nonbool['.$value.'][]" value="'.$value.'" id="'.$value.'"';
			if(array_key_exists($value,$contents)){$tableString=$tableString.' checked';} 
			$tableString=$tableString.'><label for="'.$value.'"></label></div></td>';		  			
		}
		$tableString=$tableString."</tr><tr><td style='width:100px;'><p style='width:100px;'>EPA MCL (mg/L)</p></td>";
		foreach ($_SESSION['contaminants'] as $key => $value) {
			$tableString=$tableString.'<td><input type="text" size="7" name="nonbool['.$value.'][]" value="'.$contents[$value].'" style="height:2em;"></td>';		  			
		}
		echo $tableString."</tr></table>";
		echo'<input type="submit" name="nonboolsubmit" value="submit" class="loginButton"></form>';
	}


	function picture_table_to_form($table, $target) {
		$mysqli = new mysqli(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_NAME);
		echo '<form action='.$target.' method="post" enctype="multipart/form-data">';
		$query = "SELECT * FROM ".$table." LIMIT 1000";
		$result = $mysqli->query($query);

		//finding which column has the uid/primary (can't be changed)
		$query = "SELECT COLUMN_NAME FROM KEY_COLUMN_USAGE WHERE TABLE_NAME = '".$table."'";
		$super_mysqli = new mysqli(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_DESC_NAME);
		$uid_res = $super_mysqli->query($query);
		$arr = $uid_res->fetch_assoc();
		$uid_str = $arr["COLUMN_NAME"];
		$uid_name = $uid_str;
		$uid_ind = 0;
		
		$desc_res = $mysqli->query("DESCRIBE ".$table);
		$desc_res2 = $desc_res;
		echo'<table id="admin_table" class="'.$table.'" width="100%">';
		echo'<tr>';
		$desc;
		echo '<td id="admin_col_header"><b>'.$uid_name.'</b></td>';
		$desc[] = $uid_name;
		$col_names[] = $uid_name;
		while($row = $desc_res->fetch_row()){//loops thru all the column names
			if ($uid_str != ""){//finding the index of the column that is the uid
				if ($uid_str == $row[0]){
					$uid_str = "";
				} else {
					$uid_ind ++;
				}
			}
			if ($row[0] != $uid_name){
				echo '<td id="admin_col_header"><b>';
				if ($row[0]!="url"){
					printf($row[0]);	
				}else{
					echo "Current Picture";
				}
				$desc[] = $row[0];
				echo '</b></td>';
				$col_names[] = $row[0];
			}
		}echo "<td id='admin_col_header' width='200px'><b>Add/Change Photo</td>";
		echo'<td style="width:100px;"></td></tr>';
		while ($row = $result->fetch_row()){
			//$row[0]
		 	echo'<tr>';
		 	//put the uid first
		 	$uid_val = $row[$uid_ind];
		 	echo '<td>'.$uid_val.'</td>';//note uid cannot be changed

		 	for ($i = 0; $i < count($row); $i++){
		 		//DATA
		 		if ($i != $uid_ind){
		 			echo '<td>';
		 			if($desc[$i]=="url"){
		 				echo '<img src='.$row[$i].' alt="'.$row[$i-1].'" height="100";/> ';
		 			}else{
						echo '<input type="text" size="16" name="'.$row[$uid_ind].'_-_'.$desc[$i].'_--_'.$uid_name.'" value="'.$row[$i].'">';
		 			}echo ' </td> ';
		 		}
		 	}
		 	echo"<td style='width:100px;'>
						<input type='file' name='".$row[$uid_ind]."_-_newphoto' id='browse'>
				</td>";
		 	echo'</tr>';
		}
		echo '<tr id="row_for_spacing"><td><p> </p></td></tr><tr>';

		echo '</tr>';
		echo'</table>';
		echo'<input type="submit" name="submit" value="submit" class="loginButton">
			</form>';
		return 0;
	}
	/**
	*table is the table from which you pull
	*/
	function table_to_table($table) {
		$mysqli = new mysqli(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_NAME);
		$query = "SELECT * FROM ".$table." LIMIT 1000";
		$result = $mysqli->query($query);

		//finding which column has the uid/primary (can't be changed)
		$query = "SELECT COLUMN_NAME FROM KEY_COLUMN_USAGE WHERE TABLE_NAME = '".$table."'";
		$super_mysqli = new mysqli(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_DESC_NAME);
		$uid_res = $super_mysqli->query($query);
		$arr = $uid_res->fetch_assoc();
		$uid_str = $arr["COLUMN_NAME"];
		$uid_name = $uid_str;
		$uid_ind = 0;
		
		$desc_res = $mysqli->query("DESCRIBE ".$table);
		$desc_res2 = $desc_res;
		echo'<table id="admin_table">';
		echo'<tr>';
		$desc;
		while($row = $desc_res->fetch_row()){//loops thru all the column names
			echo '<td id="admin_col_header">';
			printf($row[0]);
			$desc[] = $row[0];
			echo '</td>';
			$col_names[] = $row[0];
		}
		echo'</tr>';
		while ($row = $result->fetch_row()){
		 	echo'<tr>';
		 	for ($i = 0; $i < count($row); $i++){
		 		if($table=="regression_params"){echo'<td id="'.$col_names[$i].'">';}else{echo '<td>';}
		 		echo $row[$i];
		 		echo ' </td> ';
		 	}
		 	echo'</tr>';
		}
		echo'</table>';
		return 0;
	}

	/**
	*shows the user, can't update the username, only the password only for one user
	*/
	function user_table_display() {
		echo "<p class='explanatoryText'><br/>Update the administrator password to the Selection Guide by entering a new password here.</p>";
		$table = "users";
		$mysqli = new mysqli(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_NAME);
		$query = "SELECT username, password FROM users LIMIT 1000";
		$result = $mysqli->query($query);
		echo '<form action=admin.php?table=users method="post">';

		//finding which column has the uid/primary (can't be changed)
		$query = "SELECT COLUMN_NAME FROM KEY_COLUMN_USAGE WHERE TABLE_NAME = '".$table."'";
		$super_mysqli = new mysqli(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_DESC_NAME);
		$uid_res = $super_mysqli->query($query);
		$arr = $uid_res->fetch_assoc();
		$uid_str = $arr["COLUMN_NAME"];
		$uid_name = $uid_str;
		$uid_ind = 0;
		echo'<table id="admin_table">';
		$row = array("username","old password", "new password", "repeat new password");
		echo'<tr>';
		foreach ($row as $key) {//loops thru all the column names
		 	echo '<td id="admin_col_header">';
		 	echo "<b>".$key."</b>";
		 	echo '</td>';
		}
		echo'</tr>';
		// //print out all the data as cells
		echo'<tr>';
		while ($row = $result->fetch_row()){
		 	for ($i = 0; $i < count($row); $i++){
		 		echo '<td>';
		 		//DATA
		 		if ($i == $uid_ind){
		 			echo $row[$i];//uid can't be changed
		 		} else {
			 		echo '<input type="password" name="'.$row[$uid_ind].'" required>';
		 		}
		 		echo ' </td> ';
		 	}
		}//prints out the username and textbox for old password
 		echo '<td><input type="password" name="new_pass_1" required></td>';
 		echo '<td><input type="password" name="new_pass_2" required></td>';
		echo'</tr>';
		echo'</table>';
		echo'<input type="submit" name="submit_admin_password" value="submit" class="loginButton">
			</form>';
		return 0;
	}

	if (isset($_GET["logout"]) && $_GET["logout"] == "true"){
		unset($_SESSION["user"]);
	}

	$mysqli = new mysqli(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_NAME);
	
	/* check connection */
	if ($mysqli->connect_errno) {
	    printf("Connect failed: %s\n", $mysqli->connect_error);
	    exit();
	}
	
	if (isset($_POST['username'] ) && isset( $_POST['password'] ) ){

		
		$mysqli = new mysqli(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_NAME);

		// clean up input
		$username = $mysqli->real_escape_string(strip_tags( $_POST['username'] ));

		//find the salt for the user
		$salt = "";
		if ($salt_result = $mysqli->query("SELECT salt FROM users WHERE username ='$username'")){
			$salt_arr = ($salt_result->fetch_assoc());
			$salt = $salt_arr['salt'];
		}
		//salt is good
		$password = hash("sha256",$salt.$_POST['password']);

		//Check for a record that matches the POSTed credentials
		$query = "SELECT * FROM users WHERE username = '$username' AND password = '$password' AND salt = '$salt'";

		$result = $mysqli->query($query);
		//printf($result->num_rows);
		if ( $result && $result->num_rows == 1) {
			$row = $result->fetch_assoc();
			$_SESSION["user"] = $row['username'];
		} 
	}
	
	/*This function determines whether an uploaded file (passed in in the format $_FILES['newphoto']
	is a valid image file. */
	function validPic(&$thefilehere) {
		$allowedExts = array("gif", "jpeg", "jpg", "png", "JPG");
		$temp = explode(".", $thefilehere["name"]);
		$extension = end($temp);
		$placeholder = ((($thefilehere["type"] == "image/gif")
		|| ($thefilehere["type"] == "image/jpeg")
		|| ($thefilehere["type"] == "image/jpg")
		|| ($thefilehere["type"] == "image/JPG")
		|| ($thefilehere["type"] == "image/pjpeg")
		|| ($thefilehere["type"] == "image/x-png")
		|| ($thefilehere["type"] == "image/png"))
		&& in_array($extension, $allowedExts));
		return $placeholder;
	}

	function submitToDB($table){
			$mysqli = new mysqli(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_NAME);
			
			foreach ($_POST as $key => $value) {

				if ($value != "" && $value != "submit" && substr($key,0,7)!="addrow-" ) {
					$submits[$key] = $value;
					
				} else if ($value != "" && $value != "submit" && substr($key,0,7) == "addrow-") {
					$add[substr($key,7)] = $value;
					
				} //else represents all the blank values
			}

			if (isset($add)){//if the blank additional row is empty
				$query_str = 'INSERT INTO '.$table. ' (';
				$vals = ') VALUES (';
				
				foreach($add as $col => $val) {
					$query_str = $query_str.$col.', ';
					$vals = $vals.'"'.$val.'", ';
				}
				$vals = substr($vals,0,strlen($vals)-2);
				$query_str = substr($query_str,0,strlen($query_str)-2);
				$query_str = $query_str.$vals.')';
				//send to DB
				echo $query_str;
				$mysqli->query($query_str);
				if($table=="option_text"){
					// echo "query into constraints";
					// echo $add[0];
				}else if($table=="constraints"){
					// echo "query into option_text";
					// echo $add[0];
					
					contaminantsSession();
					// print_r($_SESSION['contaminants']);
				}
			}
			foreach ($submits as $key => $value) {
				//value is the new value to put in at uid_-_column
				$uid = substr($key,0,strpos($key,'_-_'));
				$temp = substr($key,3+strpos($key,'_-_'));
				$col = substr($temp,0,strpos($temp,'_--_'));
				$uid_col = substr($temp,4+strpos($temp,'_--_'));
				$col = str_replace("_", " ", $col);
				$query = 'UPDATE '.$table.' SET `'.$col.'`="'.$value.'" WHERE `'.$uid_col.'`="'.$uid.'"';
				// echo $query;
				$mysqli->query($query);
			}
		}


	if (isset($_SESSION["user"])) {//user is logged in
		$user = $_SESSION['user'];
		$super_mysqli = new mysqli(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_DESC_NAME);
		$names = array(
			"constraints" => "Treatment Option Constraints",
			"regression" => "Historical Cost/Flow Rate",
			"userdata" => "View User Data",
			"users" => "Update Password",
			"option_text" => "Edit Text Descriptions",
			"pictures" => "Upload Recommendation Photos"
			);
		if (mysqli_connect_errno()) {
    		printf("Connect failed: %s\n", mysqli_connect_error());
    	}
		$query = 'SELECT TABLE_NAME FROM TABLES WHERE TABLE_SCHEMA LIKE \''.DB_NAME.'\'';
		$res = $super_mysqli->query($query);

		if(!isset($_GET["table"])){
			echo '<h2> Hello! Welcome to the administrative panel for the Water Treatment Technology Selection Guide.</h2>';
			echo '<p class="explanatoryText">From this panel, you can view and update the guide\'s data. Select an option below to view its data.</p><br/>';
			
		}
		echo '<ul class="list_of_tables_in_db"> ';
		if(isset($_GET["table"])){
			echo  "<a href='admin.php'><li class='a_table_in_db'>Admin Home</li></a>";
		}
		//These are the links across the top of the pane
		while($row = $res->fetch_row()){
			if(isset($_GET["table"])){$table_selected = $_GET["table"];}
			$tblName = $row[0];

			if($table_selected==$tblName){
				echo '<a href="admin.php?table='.$tblName.'"><li class="a_table_in_db" style="color:blue;">'.$names[$tblName].'</li></a>';
			}else if($tblName != "regression_params" && $tblName != "microbes" && $tblName!= "nonboolean_contaminants"){
				echo '<a href="admin.php?table='.$tblName.'"><li class="a_table_in_db">'.$names[$tblName].'</li></a>';
			}
			
		}

		//handle submission to changes of a form
		
		if (in_array("delete",$_POST) && "delete" == substr(array_search("delete",$_POST),0,strlen("delete")) && isset($_GET["table"])){
			$mysqli = new mysqli(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_NAME);
			$table = $mysqli->real_escape_string(strip_tags($_GET["table"]));
			$key = array_search("delete",$_POST);
			$uid = substr($key,0,strpos($key,'_-_'));
			$temp = substr($key,3+strpos($key,'_-_'));
			$col = substr($temp,0,strpos($temp,'_--_'));
			$uid_col = substr($temp,4+strpos($temp,'_--_'));
			$uid_col = str_replace("_", " ", $uid_col);
			//echo 'uid'.$uid.'temp'.$temp.'col'.$col.'uid_col'.$uid_col;
			$query = 'DELETE FROM '.$table.' WHERE '.$col.'="'.$uid_col.'"';
			$mysqli->query($query);

			if ($table=="contaminants"){
				$query = 'DELETE FROM `microbes` WHERE '.$col.'="'.$uid_col.'"';
				$mysqli->query($query);
				contaminantsSession();
				microbesSession();
				nonboolRemovedSession();
			}
			//updates the regression_params based off the updated regression info
			if ($table == "regression"){
				//echo 'proc regression stuffs:';
			 	$flow_query = "SELECT FlowRate FROM regression ORDER BY PlantName ASC";
			 	$cost_query = "SELECT CostPerLS FROM regression ORDER BY PlantName ASC";
				$mysqli = new mysqli(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_NAME);
			 	if (mysqli_connect_errno()) {
	    			printf("Connect failed: %s\n", mysqli_connect_error());
	    		}
			 	//$flows = array(16,32,32,35,44);
			 	$flow_result = $mysqli->query($flow_query);
			 	//print_r($flow_result);
			 	while ($row = $flow_result->fetch_array()) {
			 		$flows[] = $row[0];
			 	}
			 	//$costss = array(11777,7486,7722,6867,6645);
			 	$cost_result = $mysqli->query($cost_query);
			 	while ($row = $cost_result->fetch_array()) {
			 		$costs[] = $row[0];
			 	}

			 	//print_r($flows);
			 	//print_r($costs);
			 	$arr = (cost_regression($flows,$costs));
			 	$a = $arr[0];
			 	$b = $arr[1];
				//printf($a.'    '.$b);
				//hard coded a=61076.557238634 b=-0.59945041710366
				//for inserting alpha and beta back intp the regression_params table
				$delete = 'DELETE FROM regression_params';
				$mysqli->query($delete);
				$insert = 'INSERT INTO regression_params (alpha, beta) VALUES ('.$a.', '.$b.')';
				$mysqli->query($insert);
			} 
		}
		if (isset($_POST["submit"]) && isset($_GET["table"])){
			//PUT THE FUNCTION HERE
			$table = $mysqli->real_escape_string(strip_tags($_GET["table"]));
			submitToDB($table);
			if ($table=="constraints"){
				submitToDB("nonboolean_contaminants");
				submitToDB("microbes");
			}


			//updates the regression_params based off the updated regression info
			if ($table == "regression"){
				//echo 'proc regression stuffs:';
			 	$flow_query = "SELECT FlowRate FROM regression ORDER BY PlantName ASC";
			 	$cost_query = "SELECT CostPerLS FROM regression ORDER BY PlantName ASC";
				$mysqli = new mysqli(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_NAME);
			 	if (mysqli_connect_errno()) {
	    			printf("Connect failed: %s\n", mysqli_connect_error());
	    		}
			 	//$flows = array(16,32,32,35,44);
			 	$flow_result = $mysqli->query($flow_query);
			 	//print_r($flow_result);
			 	while ($row = $flow_result->fetch_array()) {
			 		$flows[] = $row[0];
			 	}
			 	//$costss = array(11777,7486,7722,6867,6645);
			 	$cost_result = $mysqli->query($cost_query);
			 	while ($row = $cost_result->fetch_array()) {
			 		$costs[] = $row[0];
			 	}

			 	//print_r($flows);
			 	//print_r($costs);
			 	$arr = (cost_regression($flows,$costs));
			 	$a = $arr[0];
			 	$b = $arr[1];
				//printf($a.'    '.$b);
				//hard coded a=61076.557238634 b=-0.59945041710366
				//for inserting alpha and beta back intp the regression_params table
				$delete = 'DELETE FROM regression_params';
				$mysqli->query($delete);
				$insert = 'INSERT INTO regression_params (alpha, beta) VALUES ('.$a.', '.$b.')';
				$mysqli->query($insert);
			} 
		} else if (isset($_POST["submit_admin_password"]) && isset($_GET["table"])) {//for changing the password
			$table = $_GET["table"];
			$new_pass = "";
			foreach ($_POST as $key => $value) {
				//echo '... key'.$key.', value:'.$value;
				$updatequery = 'UPDATE users SET password="';//only for doing one passwordupdate with username admin
				if ($value != "" && $value != "submit" && $key == "admin") {
					$oldpass = $value;
					$found = 1;
				} else if ($value != "" && $value != "submit"){
					if ($new_pass == "") {
						$new_pass = $value;
						$match = 0;
					} else if ($new_pass != $value) {
						echo' <p style="color:red;"><br/>ERROR: new passwords do not match</p>';
						$match = 0;
					} else {
						$match = 1;
					}
				}
			}
			//find the salt for the user
			$salt = "";
			$mysqli = new mysqli(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_NAME);
			if ($salt_result = $mysqli->query('SELECT salt FROM users WHERE username ="admin"')){
				$salt_arr = ($salt_result->fetch_assoc());
				$salt = $salt_arr['salt'];
			}
			//salt is good
			$password = hash("sha256",$salt.$oldpass);
			//Check for a record that matches the POSTed credentials
			$query = 'SELECT * FROM users WHERE username = "admin" AND password = "'.$password.'" AND salt = "'.$salt.'"';

			$result = $mysqli->query($query);
			//printf($result->num_rows);
			if ( $result && $result->num_rows == 1) {
				$found = 1;
			} else {
				$found = 0;
				echo'<p style="color:red;"><br/>ERROR: incorrect old password</p>';
			}
			if ($found == 1 && $match == 1) {//found is 1 iff old pass was entered and is correct, match is 1 iff new passwords match
				$newsalt = generateRandomString();
				$newpass = hash("sha256",$newsalt.$new_pass);
				$updatequery = $updatequery.$newpass.'", salt="'.$newsalt.'" WHERE username = "admin"';
				$mysqli->query($updatequery);
			}
		}


		if (isset($_GET["table"])) {
			//assuming that table is set to a valid table in the database
			$table = $_GET["table"];
			
			if($table=="constraints"){
				if(isset($_POST['nonbool'])){
					// echo var_dump($_POST['nonbool']);
					$mysqli->query("TRUNCATE `wttsg`.`nonboolean_contaminants`;");
					$query = "INSERT into `wttsg`.`nonboolean_contaminants` (`contaminantName`, `mgPerLConc`) VALUES ";
					
					//Can't submit to the DB if one or more of the two fields is empty
					$no_empties = [];
					foreach ($_POST['nonbool'] as $key => $value) {

						if ((strlen($value[0])!=0)  && (strlen($value[1])!=0)){
							$no_empties[] = array($value[0], $value[1]);
						}
						//DEFAULT VALUE OF ZERO FOR CONTAMINANTS SELECTED W/O VALUE
						else if ((strlen($value[0])!=0) ){
							$no_empties[] = array($value[0], 0);
						}
					}
					// echo "<br/><br/>";
					// echo var_dump($no_empties);
					// echo "<br/><br/>";
					foreach ($no_empties as $key => $value) {
						$query = $query.'("'.$value[0].'", "'.round($value[1],5).'")';
						if($key!=(count($no_empties)) - 1){
							$query=$query.", ";
						}
					}	
					// echo $query;			
					$mysqli->query($query);
				}

				echo '<br/><p class="explanatoryText"><br/>Edit constraints for various treatment options here. These are used in the tool\'s decision logic.</p>';
				table_to_form($table,"admin.php?table=".$table);

				echo '<br/><p class="explanatoryText"><br/>Which of the following contaminant fields require quantitative (non-boolean) data?</p>';
				nonbool_const_form("nonboolean_contaminants","admin.php?table=".$table);

				echo '<br/><p class="explanatoryText"><br/>Edit microbe constraints for various treatment options here.</p>';
				table_to_form("microbes","admin.php?table=".$table);

			}else if ($table=="regression"){
				echo'<p class="explanatoryText"><br/>Review the regression parameters which relate flow rate and plant cost using a power equation.</p>';
				table_to_table("regression_params");

				echo'<script src="http://d3js.org/d3.v3.min.js" charset="utf-8"></script>
				<style>
					.axis path { fill: none; stroke: black;}
					.axis line { stroke: black; }
					.axis text { font-size: x-small; }
					path {
						stroke: black;
						stroke-width: 2;
						fill: none;
					}
				</style>
				<div id="regressionVisualization"></div>
				<script>
						var data = [],
					    n = 100,
					    a = parseFloat(document.getElementById("alpha").innerHTML),
					    b = parseFloat(document.getElementById("beta").innerHTML);

					    var maxFlow = 40;
						for (var k = 7; k < maxFlow; k++) {
						    data.push({x:  k, y: a * Math.pow(k,b)});
						    console.log(a*Math.pow(k,b));
						}

						var height = 250;
						var width = 400;
						var padding = 30;

						var svg = d3.select("#regressionVisualization").append("svg")
							.attr("height", height).attr("width", width);

						var xScale = d3.scale.linear()
							.domain([0, maxFlow])
							.range([padding, width - padding]);

						var yScale = d3.scale.pow()
							.domain([0, 20000])
							.range([height - padding, padding]);
						
						var xAxis = d3.svg.axis().scale(xScale);
						svg.append("g")
						  .attr("class", "axis")
						  .attr("transform", "translate(0, " + (height - padding) + ")")
						  .call(xAxis);

						var yAxis = d3.svg.axis().scale(yScale).orient("left");
						svg.append("g")
							.attr("class", "axis")
							.attr("transform", "translate(" + padding + ", 0)")
							.call(yAxis);

						var line = d3.svg.line()
							.x(function(d) { 
								return xScale(d.x); 
							})
							.y(function(d) { 
								return yScale(d.y); 
							})
							;

						svg.append("path").attr("d", line(data));

						/*svg.selectAll("circle").data(data).enter().append("circle")
							.attr("cx", function(d){return xScale(d.x);})
							.attr("cy", function(d){return yScale(d.y);})
							.attr("r", 2)*/
						</script>
				';

				echo '<br/><p class="explanatoryText"><br/>Enter in data from existing plants to help train the plant cost regression.</p>';
				table_to_form($table,"admin.php?table=".$table);
				
			}else if ($table=="pictures"){
				echo'<br/><p class="explanatoryText"><br/>Upload and review the photos that will accompany a recommendation of this treatment option.</p>';
				
				
				// echo var_dump($_POST);
				$mysqli = new mysqli(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_NAME);
				$table = $mysqli->real_escape_string(strip_tags($_GET["table"]));
				$key = array_search("Upload Photo",$_POST);
				$uid = substr($key,0,strpos($key,'_-_'));
				$temp = substr($key,3+strpos($key,'_-_'));
				$col = substr($temp,0,strpos($temp,'_--_'));
				$uid_col = substr($temp,4+strpos($temp,'_--_'));
				$uid_col = str_replace("_", " ", $uid_col);
				// echo 'uid'.$uid.'temp'.$temp.'col'.$col.'uid_col'.$uid_col;
				
				
				// echo var_dump($_FILES);
				// $row[$uid_ind]"_-_"
				// if something has been uploaded and it is a picture we will proceed
				if (!empty( $_FILES ))
				{
					$accToPrint = "";
					foreach($_FILES as $key=>$pictureFile)
					{
						if($pictureFile['error']==0 && validPic($pictureFile)) #no upload error, and valid pic
						{ 							
							$mysqli = new mysqli(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_NAME);		
							$originalName = $pictureFile['name'];
							$tempName = $pictureFile['tmp_name']; 
							move_uploaded_file( $tempName, "uploads/".$originalName);
							$_SESSION['photos'][] = $originalName;
							$accToPrint=$accToPrint."<p>The file $originalName was uploaded successfully.<br>\n</p>";

							$treatmentOption = substr($key, 0, strpos($key,'_-_'));
							
							$altText = $mysqli->real_escape_string(strip_tags($_POST[$treatmentOption.'_-_altText_--_treatmentOption']));
							// add the picture uploaded to the photos table
							$addquery = 'REPLACE INTO `pictures` (`treatmentOption`, `altText`, `url`) 
							VALUES ("'.$treatmentOption.'", "'.$altText.'", "uploads/'.$originalName.'");' ;
							// echo $addquery;	
							$result = $mysqli->query($addquery);
						}elseif ($pictureFile['error']==0 && !validPic($pictureFile )){
							$originalName = $pictureFile['name'];
							$accToPrint=$accToPrint."<p>Error: The file $originalName is not an image of an acceptable type. Please try again with a different photo.\n</p>";							
						}
					}
				}
				picture_table_to_form($table, "admin.php?table=".$table);
				echo $accToPrint;

			}else if ($table == "users") {
				user_table_display();
			}else if ($table == "userdata"){
				write_userdata_to_file();
				echo'<br/><p class="explanatoryText"><br/>Review the data input by those who have visited the Selection Guide.</p>
				<div style="padding-top:1em;">
						<p><a href="userdata.php" style="color:black;font-size:1.4em;">Download userdata as a CSV</a></p></div>';
			}else {
				table_to_form($table,"admin.php?table=".$table);
			}
		}//18 * 13 =  x = 
		echo '<script>var table = document.getElementById("admin_table")
						var len = $("#admin_table tr").length;
						if (table){
							var x = table.rows.length * 26+92+60+20+60; 
					 		$(".content").css("height", ""+x+"px");
					 	}
					 	var classText = document.getElementsByClassName("option_text");
					 	if(classText.length ==1){
					 		var x = (table.rows.length +1)* 78 +30; 
					 		$(".content").css("height", ""+x+"px");
					 	}
					 	var classOptions = document.getElementsByClassName("regression");
					 	if(classOptions.length ==1){
					 		var x = table.rows.length * 26+92+60+350+40 + 260; 
					 		$(".content").css("height", ""+x+"px");
					 	}
					 	var classPictures = document.getElementsByClassName("pictures");
					 	if(classPictures.length ==1){
					 		
					 		var x = (len-3) * 110 + 250; 
					 		$(".content").css("height", ""+x+"px");
					 	}
					 	var classConst = document.getElementsByClassName("constraints");
					 	if(classConst.length == 1){
					 		var miclength = document.getElementsByClassName("microbes")["admin_table"].rows.length;
					 		var nonbools = 237;
					 		var x = (table.rows.length + miclength) * 47 + 2*47 + 70 + nonbools + 70;
					 		$(".content").css("height", ""+x+"px");
					 	}
					 	</script>';
	}

	if (!isset($_SESSION["user"])){
		echo '<h2> Admin Login </h2>
			<form action="admin.php" method="post">
			<input type="text" name="username" class="logbox" placeholder="Login">
			<br/>
			<input type="password" name="password" class="logbox" placeholder="Password"> 
			<div id="loginerror"></div>
			<input type="submit" value="login" name="login" class="loginButton">
			</form>';
	}
	?>
	<script>scroll_lock('loginButton');
	sneaky("a_table_in_db");
	</script>
	<?php
include 'footer.php'
?>