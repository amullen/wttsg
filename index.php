<?php 
session_start();
if(!isset($_SESSION['uniqueUserId'])){
	$_SESSION['uniqueUserId'] = uniqid($prefix = "");
}

include 'header.php';



?>
					
					<?php
					
					//$mysqli = new mysqli(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_NAME);
					
					/*Create forward and back arrows at the bottom of the content div*/
					function arrowecho($page){
						$forward = $page + 1;
						$back = $page - 1;
						if ($page == 0){
							//case where scroll sneak needs hidden arrow button to work
							echo '
							<a href="index.php" class="arrowlink">
								<img src="assets/arrowleft.png" alt="Back" width=0 height=0 class="arrow" id="left'.$page.'"/> 
							</a>
							<input type="submit" name="p" value="'.$forward.'" class="arrow tripleArrowRight" id="right'.$page.'">'; 
						} else if ($page == 10){//handles the last page whose form goes to results.php, but the backarrow should stay at index.php
							echo '
							<input type="submit" formaction="index.php" formmethod="get" name="p" value="'.$back.'" class="arrow tripleArrowLeft" id="left'.$page.'">'; 
							echo '<input type="submit" name="p" value="'.$forward.'" class="arrow tripleArrowRight" id="right'.$page.'">';
						} else {
							echo '
							<input type="submit" formaction="index.php" formmethod="get" name="p" value="'.$back.'" class="arrow tripleArrowLeft" id="left'.$page.'">'; 
							echo '<input type="submit" name="p" value="'.$forward.'" class="arrow tripleArrowRight" id="right'.$page.'">';
						}
					}
					
					/** Code to display pages begins here */
					$page = 0;	
					if (isset($_POST["p"])){$_GET["p"]=$_POST["p"];}
					if (!isset($_GET["p"]) or $_GET["p"]==0){
						echo '<h2><span class="dramaticSpan">Welcome! </span> Please enter your information to begin.</h2>
						<form name="forms" action="index.php" method="post">';
							if (isset($_SESSION['userinputname']) && isset($_SESSION['userinputorg'])){
								echo'<input type="text" name="user" class="txtbox" placeholder="Name" value="'.$_SESSION["userinputname"].'">
							<br/>
							<input type="text" name="org" id="org" class="txtbox" placeholder="Organization" value="'.$_SESSION["userinputorg"].'">'; 
							}
							else{ echo'<input type="text" name="user" class="txtbox" placeholder="Name">
							<br/>
							<input type="text" name="org" id="org" class="txtbox" placeholder="Organization">'; 
							}
							echo '<div id="emailerror"></div>
						<div class="arrowMessage"></div>
						';
						arrowecho(0);
						echo "</form>";
						//ON CLICK, START A SESSION VARIABLE FOR THE CURRENT USER?
						//maybe if going the db route, check 
						if (isset($_GET["city"]) && isset($_GET["countries"])){
							$_SESSION["userinputcity"]=htmlentities(strip_tags( $_GET['city'] ));
							$_SESSION["userinputcountry"]=htmlentities(strip_tags( $_GET['countries'] ));
						}
						
					} else {
						$page = $_GET["p"];
					}
					if ($page=="1"){	
						//unset($_SESSION['userinputavgNTU']);
						echo '<h2><span class="dramaticSpan">Where</span> are you from?</h2>
						<form name="forms" action="index.php?p='.strval($page+1).'" method="post">';
						
						if (isset($_SESSION["userinputcity"]) && isset($_SESSION["userinputcountry"])){
							echo '<input type="text" name="city" class="txtbox" placeholder="City/Town" value="'.$_SESSION["userinputcity"].'"><br/>';
							echo'<select id="countries" name="countries" class="countrydrop">';
							//countries defined in countryarray.php
							foreach($countries as $value) {
								if ($value===$_SESSION["userinputcountry"]){
									print '<option value="'.$value.'" selected="selected">'."$value".'</option>';
								} else {
									print '<option value="'.$value.'">'."$value".'</option>';
								}	
							}
							print "</select>";
						} else {
							echo '<input type="text" name="city" class="txtbox" placeholder="City/Town"><br/>';
							echo'<select id="countries" name="countries" class="countrydrop">';
							//countries defined in countryarray.php
							foreach($countries as $value) {
								print '<option value="'.$value.'">'."$value".'</option>';
							}
							print '</select>';
						}
						echo '<br/>';
						arrowecho($page);
						echo '</form>';
						
						if (isset($_POST["user"]) && isset($_POST["org"])){
							$_SESSION["userinputname"]=htmlentities(strip_tags( $_POST['user'] ));
							$_SESSION["userinputorg"]=htmlentities(strip_tags( $_POST['org'] ));
							/*$name = $mysqli->real_escape_string(strip_tags( $_POST['name'] ));
							$org = $mysqli->real_escape_string(strip_tags( $_POST['org'] ));
							$addquery = "INSERT INTO `responses` (`name` ,`organization`)
								VALUES ('".$name."' , '".$org."');" ;
													
							$result2 = $mysqli->query($addquery);
							*/
						}
						if (isset($_GET["slide"])){
							$_SESSION["userinputpop"] = $_GET["slide"];
						}
					
					} else if ($page=="2"){

						echo '<h2><span class="dramaticSpan">How many people</span> will your treatment plant serve?</h2>';
						/*if (isset($_SESSION["userinputpop"])){
							$var = $_SESSION["userinputpop"];
							if ($var > 900) {
								$var += 7000;
								$var /=10;
							} else {
								$var -= 100;
							}
							echo'<p>'.$var.'</p>';
						}*/
						echo '
						<form name="forms" action="index.php?p='.strval($page+1).'" method="post">
						<label class="rangelab">50</label>';
						if (isset($_SESSION["userinputpop"])){
							$var = $_SESSION["userinputpop"];
							if ($var > 900) {
								$var += 7000;
								$var /=10;
							} else {
								$var -= 100;
							}
							echo '<input type=range id=population min=50 value='.$var.' 
												max=3200 step=50 name=slide oninput="outputUpdate(value)">';
						} else {
							echo '<input type=range id=population min=50 value=100 max=3200 step=50 name=slide oninput="outputUpdate(value)">';
						}
						echo '<label class="rangelab">25000</label>
						</br>';
						if (isset($_SESSION["userinputpop"])){
							if ($_SESSION["userinputpop"] > 900) {
								$inc = $_SESSION["userinputpop"] - 1000;
								echo '<output for=population id=pop>'.$inc.' - '.$_SESSION["userinputpop"].'</output>';
							} else {
								$inc = $_SESSION["userinputpop"] + 100;
								echo '<output for=population id=pop>'.$_SESSION["userinputpop"].' - '.$inc.'</output>';
							}
						} else {
							echo '<output for=population id=pop> 100 - 200 </output>';
						}
						echo '
						<!-- JavaScript code from http://demosthenes.info/blog/757/Playing-With-The-HTML5-range-Slider-Input -->
						<script>
							function outputUpdate(val) {
								if (val >= 950){
									val*=10;
									val -= 8000;
									val = (val-val%1000);
									var v2 = val + 1000;
								} else {
							  		var v2 = parseInt(val) + 100;
							  	}

							  document.querySelector("#pop").value = val + " - " + v2;
							}
						</script>
						';
						arrowecho($page);
						echo '</form>';
						
						if (isset($_POST["city"]) && isset($_POST["countries"])){
							$_SESSION["userinputcity"]=htmlentities(strip_tags( $_POST['city'] ));
							$_SESSION["userinputcountry"]=htmlentities(strip_tags( $_POST['countries'] ));
						}
						if (isset($_GET["popGrowth"])){
							$_SESSION['userinputpopgrowth'] = $_GET["popGrowth"];
						}
						if (isset($_GET["waterUsage"])){
							$_SESSION['userinputwaterusage'] = $_GET["waterUsage"];
						}
					} 
					else if ($page=="3"){
						
						echo '<h2>What is your <span class="dramaticSpan">population growth rate? (%)</span><div style="margin-top:-8px;">What is the average <span class="dramaticSpan">per capita daily water use? (Litres)</span></div></h2>';
						echo '<img src="assets/populationGrowth.png" alt="Population Growth" class="icon" /> <img src="assets/faucet.png" alt="Daily Water Usage" width="100" class="icon"/>' ;
						echo'<form name="forms" action="index.php?p='.strval($page+1).'" method="post">';
						echo '<input type="hidden" name="pageFilledOut" value="True">';
						echo '<input type="text" name="popGrowth" placeholder=".5" ';
						
						if(isset($_SESSION["userinputpopgrowth"])){echo 'value="'.$_SESSION['userinputpopgrowth'].'"';}
						echo' class="workerInput"> <input type="text" name="waterUsage" placeholder="100" ';
						if(isset($_SESSION['userinputwaterusage'])){echo 'value="'.$_SESSION['userinputwaterusage'].'"';}
						echo'class="workerInput">';
						
						arrowecho($page);
						echo '</form>';

						if (isset($_POST["slide"])){
							$var = $_POST["slide"];
							//echo '<p>'.$var.'</p>';
							if ($var > 900) {
								$var*=10;
								$var -= 7000;
								$var = ($var-$var%1000);
							} else {
								$var += 100;
							}
							//echo '<p>'.$var.'</p>';
							$_SESSION["userinputpop"] = $var;
						}
						//adds the selected averageNTU pic from next page to SESSION var
						if (isset($_GET["averageNTU"])){
							$str = $_GET["averageNTU"];
							//code used from http://stackoverflow.com/questions/6278296/extract-numbers-from-a-string
							preg_match_all('!\d+!', $str, $matches);
							$var = implode(' ', $matches[0]);
							$_SESSION['userinputavgNTU'] = $var;
						}
					}else if ($page=="4"){
						//print_r($_SESSION);
						echo '<h2><span class="dramaticSpan">Which of these</span> best approximates how dirty 
						your water <span class="dramaticSpan">normally</span> appears?</h2>';

						echo'<form name="forms" action="index.php?p='.strval($page+1).'" method="post">';
						
						$pictures = array(
							//15 => "assets/25ntu.jpg",

							30 => "assets/30ntu.jpg",
							60 => "assets/60ntu.jpg",
							100 => "assets/100ntu.jpg",
							200 => "assets/200ntu.jpg",
							500 => "assets/500ntu.jpg",
						);
						
						echo '<label for="NTU0"><div class="polaroid" data-ntus=0 id="polaroid20" ';
								//allows for showing which was chosen the last time the user entered the data
								if(isset($_SESSION['userinputavgNTU'])&& $_SESSION['userinputavgNTU'] == 0){
									echo 'style="box-shadow: rgb(79,90,94) 1px 1px 12px 5px;"';
								}
								echo'>
							<div class="polaroidp">'.'15 NTU'.'</div>
							<div class="verticalhelper"><img src="assets/15ntu.jpg" alt="'."0 NTU".'">
							</div>
						</div></label>
							<input type="checkbox" class="hiddenCheckbox" id="NTU0" name="averageNTU" value="0"/>';
						
						foreach ($pictures as $x=>$x_value){
							echo '<label for="NTU'.$x.'">
							<div class="polaroidright" data-ntus='.$x.' id="polaroidright'.$x.'" ';
								//allows for showing which was chosen the last time the user entered the data
								if(isset($_SESSION['userinputavgNTU'])&& $_SESSION['userinputavgNTU'] == $x){
									echo 'style="box-shadow: rgb(79,90,94) 1px 1px 12px 5px;"';
								}
								echo'>
								<div class="polaroidp">'.$x.' NTU</div>
								<div class="verticalhelper"><img src="'.$x_value.'" alt="'.$x.' NTUs"></div>
							</div></label>
							<input type="checkbox" class="hiddenCheckbox" id="NTU'.$x.'" name="averageNTU" value="'.$x.'"/>
							';
						}
						
						echo'
						<script>
							function selectSaved(lookup) {
								$(lookup).css("box-shadow", "1px 1px 12px 5px #4F5A5E");
							}

							polaroids();
						</script>';					
						arrowecho($page);
						echo '</form>';
						
						//adds the selected averageNTU pic from prev. page to SESSION var
						if (isset($_GET["maxNTU"])){
							$str = $_GET["maxNTU"];
							//code used from http://stackoverflow.com/questions/6278296/extract-numbers-from-a-string
							preg_match_all('!\d+!', $str, $matches);
							$var = implode(' ', $matches[0]);
							$_SESSION['userinputmaxNTU'] = $var;
						}
						if (isset($_POST["pageFilledOut"])){

							if (isset($_POST["popGrowth"]) && $_POST["popGrowth"]!=NULL){
								$_SESSION['userinputpopgrowth'] = $_POST["popGrowth"];
							}else{
								$_SESSION['userinputpopgrowth'] = .5;
							}
							if (isset($_POST["waterUsage"]) && $_POST["waterUsage"]!=NULL){
								$_SESSION['userinputwaterusage'] = $_POST["waterUsage"];
							}else{
								$_SESSION['userinputwaterusage'] = 100;
							}
						}

						
						
					} else if ($page=="5"){
						echo '<h2><span class="dramaticSpan">Which of these</span> best approximates the 
						<span class="dramaticSpan">dirtiest</span> your water gets?</h2>';
						
						$pictures = array(
							30 => "assets/30ntu.jpg",
							60 => "assets/60ntu.jpg",
							100 => "assets/100ntu.jpg",
							200 => "assets/200ntu.jpg",
							500 => "assets/500ntu.jpg",
						);
						echo'<form name="forms" action="index.php?p='.strval($page+1).'" method="post">';
						echo '<div class="polaroid2" data-ntus=0 id="polaroid20"';
								//allows for showing which was chosen the last time the user entered the data
								if(isset($_SESSION['userinputmaxNTU'])&& $_SESSION['userinputmaxNTU'] == 0){
									echo 'style="box-shadow: rgb(79,90,94) 1px 1px 12px 5px;"';
								}
								echo'>
							<div class="polaroidp">'.'15 NTU'.'</div>
							<div class="verticalhelper"><img src="assets/15ntu.jpg" alt="'."NTU example".'"></div>
						</div>';
						
						foreach ($pictures as $x=>$x_value){
							echo '<label for="NTU'.$x.'">
							<div class="polaroidright2" data-ntus='.$x.' id="polaroidright2'.$x.'"';
								//allows for showing which was chosen the last time the user entered the data
								if(isset($_SESSION['userinputmaxNTU'])&& $_SESSION['userinputmaxNTU'] == $x){
									echo 'style="box-shadow: rgb(79,90,94) 1px 1px 12px 5px;"';
								}
								echo'>
								<div class="polaroidp">'.$x.' NTU</div>
								<div class="verticalhelper"><img src="'.$x_value.'" alt="'.$x.' NTUs"></div>
							</div></label>
							<input type="checkbox" class="hiddenCheckbox" id="NTU'.$x.'" name="maxNTU" value="'.$x.'"/>
							';
						}
						echo '<script>polaroids();</script>'; //makes the shadow part work

						//echo'<input type="hidden" name="maxNTU" id="hiddenMax" value=0>';
						arrowecho($page);
						echo '</form>';
						
						//adds the selected averageNTU pic from prev. page to SESSION var
						if (isset($_POST["averageNTU"])){
							$str = $_POST["averageNTU"];
							//code used from http://stackoverflow.com/questions/6278296/extract-numbers-from-a-string
							preg_match_all('!\d+!', $str, $matches);
							$var = implode(' ', $matches[0]);
							$_SESSION['userinputavgNTU'] = $var;
						}
						//ensures that information entered on the next page is preserved if back is pressed.
						if (isset($_GET["pageFilledOut"])){
							if (isset($_GET["chemical"])){
								$_SESSION['userinputchemical'] = $_GET["chemical"];
							}else{
								$_SESSION['userinputchemical'] = ["no substances or chemicals"];
							}
						}	
					} else if ($page=="6"){
						//the boolean fields!
						$contents = $_SESSION['boolContam'];

						echo '<h2><span class="dramaticSpan">What substances or chemicals</span> have been identified as a problem in your water source? Check all that apply.</h2>';
						echo'<form name="forms" action="index.php?p='.strval($page+1).'" method="post">';
						echo'<input type="hidden" name="pageFilledOut" value="True">

						<table>';
						$tableString = "";
						
				  		for ($i=0; $i< count($contents); $i++){
				  			$valueID = $contents[$i];
				  			if($i%3==0){$tableString=$tableString."<tr>";}
				  			$tableString = $tableString.'<td class="check"><div class="squaredTwo"><input type="checkbox" name="chemical[]" value="'.$valueID.'" id="'.$valueID.'"';
				  			if(isset($_SESSION['userinputchemical']) && in_array($valueID,$_SESSION['userinputchemical'])){$tableString=$tableString.' checked';} 
							$tableString=$tableString.'><label for="'.$valueID.'"></label></div></td><td style="width:260px;"><div class="pl">'.$valueID.'</div></td>';
				  			if($i%3==2){$tableString=$tableString."</tr>";}
				  		}
						$tableString=$tableString."</table>";
						echo $tableString;

						arrowecho($page);
						echo '</form>';
						//adds the selected averageNTU pic from prev. page to SESSION var
						if (isset($_POST["maxNTU"])){
							$str = $_POST["maxNTU"];
							//code used from http://stackoverflow.com/questions/6278296/extract-numbers-from-a-string
							preg_match_all('!\d+!', $str, $matches);
							$var = implode(' ', $matches[0]);
							//echo $var;
							$_SESSION['userinputmaxNTU'] = $var;
						}
						// if (isset($_GET["numOps"])){
						// 	$_SESSION["userNumberOfOperators"] = $_GET["numOps"];
						// }
						// if (isset($_GET["wages"])){
						// 	$_SESSION["userWages"] = $_GET["wages"];
						if (isset($_GET["pageFilledOut"])){
							if (isset($_GET["nonbool"])){
								$_SESSION['userinputnonbool'] = $_GET["nonbool"];
							}else{
								$_SESSION['userinputnonbool'] = ["no nonboolean substances or chemicals"];
							}
						}
							
					
					}else if ($page=="7"){
						//The quantitative fields!
						$contents = $_SESSION["nonboolContam"];
						
						echo '<h2><span class="dramaticSpan">What substances or chemicals</span> have been identified as a problem in your water source? Quantify the contamination level.</h2>';
						echo'<form name="forms" action="index.php?p='.strval($page+1).'" method="post">';
						echo'<input type="hidden" name="pageFilledOut" value="True"><table style="width:75%">';

						//JAVASCRIPT: ADD THE BOXES AS THE THINGS ARE CHECKED
						$tableString = '<script>

						function checkAddress(checkbox)
						{
						    if (checkbox.checked)
						    {
						        $("input."+checkbox.value).show();
						        $("."+checkbox.value).show();
						    }else{
						     	$("input."+checkbox.value).hide();
						     	$("."+checkbox.value).hide();
						    }
						}

				        </script>';

						// $tableString = "";

				  		for ($i=0; $i< count($contents); $i++){
				  			$valueID = $contents[$i][0];
				  			$mgPL = $contents[$i][1];
				  			
				  			if($i%2==0){$tableString=$tableString.'<tr>';}
				  			$tableString=$tableString.'<td class="check" style="vertical-align:top;height:90px;"><div class="squaredTwo" style="vertical-align:top;"><input type="checkbox" name="nonbool[]" value="'.$valueID.'" id="'.$valueID.'"  ';
				  			if(isset($_SESSION['userinputnonbool']) && in_array($valueID,$_SESSION['userinputnonbool'])){$tableString=$tableString.' checked';} 
							$tableString=$tableString.' onclick="checkAddress(this)" onload="checkAddress(this)" ><label for="'.$valueID.'"></label></div></td>';
				  			$tableString = $tableString.'<td style="width:200px;padding-bottom:20px;vertical-align:top;"><div class="pl" style="width:200px;font-size:1.5em;">'.$valueID.'<p style="font-size:.6em;">The EPA\'s maximum contaminant level (MCL) for '.$valueID.' is '.round($mgPL,2).' mg/L</p></div></td>';
				  			$tableString = $tableString.'<td style="width:200px;padding-bottom:20px;vertical-align:top;"><input type="text" maxlength="8" size="8" style="height:2em;float:left;" name="nonbool['.$valueID.']" class="'.$valueID.'"';
				  			if(isset($_SESSION['userinputnonbool']) && in_array($valueID,$_SESSION['userinputnonbool']) && count($_SESSION["userinputnonbool"][$valueID])>0){
				  				$tableString=$tableString.' value="'.round(floatval($_SESSION["userinputnonbool"][$valueID]),5).'" ';
				  			}
				  			$tableString = $tableString.'><span style="font-size:1.4em;float:left;padding-left:10px;" class="'.$valueID.'"> mg/L</span><br/></td>';
				  			if($i%2==1){$tableString=$tableString.'</tr>';}
				  		}
						$tableString=$tableString."</table>";
						$tableString=$tableString.'<script>$("input:checkbox").each(function(){checkAddress(this);});</script>'; //Hide boxes onload if not checked
						echo $tableString;


						arrowecho($page);
						echo '</form>';
						
						if (isset($_POST["pageFilledOut"])){
							if (isset($_POST["chemical"])){
								$_SESSION['userinputchemical'] = $_POST["chemical"];
							}else{
								$_SESSION['userinputchemical'] = ["no substances or chemicals"];
							}
						}

						if (isset($_GET["pageFilledOut"])){
							if (isset($_GET["microbes"])){
								$_SESSION['userinputmicrobes'] = $_GET["microbes"];
							}else{
								$_SESSION['userinputmicrobes'] = ["no bacteria, viruses, Cryptosporidium, or Giardia"];
							}
						}	
					

					}else if ($page=="8"){
						$mysqli = new mysqli(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_NAME);
						$desc_res = $mysqli->query("DESCRIBE `microbes`;");
						$microbes = [];
						while($row = $desc_res->fetch_row()){
						    $microbes[] = $row[0];
						}
						$microbes = array_diff($microbes, ["treatmentOption"]); 
						$microbes = array_values($microbes);

						echo '<h2>Which of the following, if any, are <span class="dramaticSpan">problems in your water?</span></h2>';
						echo '<img src="assets/microbes.png" alt="Ground Water" class="icon" style="display:inline-block;vertical-align:top;padding-top:40px;padding-right:60px;"/>';
						echo'<form name="forms" action="index.php?p='.strval($page+1).'" method="post" style="display:inline-block;">';
						echo'<input type="hidden" name="pageFilledOut" value="True">';
						echo '<table style="display:inline-block;">';
						$tableString = "";
						
				  		for ($i=0; $i< count($microbes); $i++){
				  			$display = $microbes[$i];
				  			$valueID = $display;
				  			$tableString = $tableString.'<tr><td class="check"><div class="squaredTwo"><input type="checkbox" name="microbes[]" value="'.$valueID.'" id="'.$valueID.'"';
				  			if(isset($_SESSION['userinputmicrobes']) && in_array($valueID,$_SESSION['userinputmicrobes'])){$tableString=$tableString.' checked';} 
							$tableString=$tableString.'><label for="'.$valueID.'"></label></div></td><td style="width:260px;"><div class="pl">'.$display.'</div></td></tr>';
				  			
				  		}
						$tableString=$tableString."</table>";
						echo $tableString;

						arrowecho($page);
						echo '</form>';

						if (isset($_POST["pageFilledOut"])){
							if (isset($_POST["nonbool"])){

								$_SESSION['userinputnonbool'] = $_POST["nonbool"];
							}else{
								$_SESSION['userinputnonbool'] = ["no nonboolean substances or chemicals"];
							}
						}
						if (isset($_GET["waterType"])){
							$_SESSION["userWaterSourceType"] = $_GET["waterType"];
						}

					}else if ($page=="9"){/*
						echo '<h2><span class="dramaticSpan">How many operators</span> will your plant have? <div style="margin-top:-8px;">What are their <span class="dramaticSpan">monthly wages?</span></div></h2>';
						echo '<img src="assets/worker.png" alt="Plant Operator" class="icon" /> <img src="assets/money.png" alt="Monthly Wages" width="100" class="icon"/>' ;
						echo '<form name="forms" action="index.php?p='.strval($page+1).'" method="post">';
						echo '<input type="text" name="numOps" placeholder="3" ';
						if(isset($_SESSION["userNumberOfOperators"])){echo 'value="'.$_SESSION["userNumberOfOperators"].'"';}
						echo' class="workerInput"> <input type="text" name="wages" placeholder="$" ';
						if(isset($_SESSION["userWages"])){echo 'value="'.$_SESSION["userWages"].'"';}
						echo'class="workerInput">';
						arrowecho($page);
						echo '</form>';*/
						
						echo '<h2><span class="dramaticSpan">What type of source water</span> will your plant use?</h2>';
						echo '<img src="assets/groundWater.png" alt="Ground Water" class="icon" /> <img src="assets/surfaceWater.png" alt="Surface Water" width="100" class="icon"/>' ;
						echo '<form name="forms" action="index.php?p='.strval($page+1).'" method="post">';

						echo '<h2 style="display:inline-block;padding:20px;text-align:center;width:140px;">Groundwater</h2>';
						echo '<h2 style="display:inline-block;padding:20px;text-align:center;width:140px;">Surface Water</h2><br/>';
						echo '<div style="width:140px;display:inline-block;padding-left:20px;padding-right:20px;margin-top:-10px;">
						
						<input type="radio" name="waterType" value="groundwater" style="width:2em;height:2em;" ';
						if (isset($_SESSION["userWaterSourceType"]) && $_SESSION["userWaterSourceType"]=="groundwater"){
							echo "checked";
						}
						echo '></div>
						<div style="width:140px;display:inline-block;padding-left:20px;padding-right:20px;margin-top:-10px;">
						<input type="radio" name="waterType" value="surface water" style="width:2em;height:2em;" ';
						if (isset($_SESSION["userWaterSourceType"])){
							if ($_SESSION["userWaterSourceType"]=="surface water"){
								echo "checked";
							}
						}else{echo "checked";}
						echo '></div>';
						arrowecho($page);
						echo '</form>';

						/*if (isset($_POST["numOps"])){
							$_SESSION["userNumberOfOperators"] = $_POST["numOps"];
						}
						if (isset($_POST["wages"])){
							$_SESSION["userWages"] = $_POST["wages"];
						}*/
						if (isset($_POST["pageFilledOut"])){
							if (isset($_POST["microbes"])){
								$_SESSION['userinputmicrobes'] = $_POST["microbes"];
							}else{
								$_SESSION['userinputmicrobes'] = ["no bacteria, viruses, Cryptosporidium, or Giardia"];
							}
						}	

						if (isset($_GET["distributionType"])){
							$_SESSION["userDistributionType"] = $_GET["distributionType"];
						}

					}  else if ($page=="10"){
						echo '<h2>Do you have a <span class="dramaticSpan">distribution system</span> in place?</h2>';
						
						echo '<form name="forms" action="results.php?results='.$_SESSION['uniqueUserId'].'" method="post">';
						echo '<div style="width:60%;margin:auto;">';
						echo'<h2 style="float:left;margin-top:20px;font-size:1.2em;"><input type="radio" name="distributionType" 
						value="YES" style="width:2em;height:2em;" ';
						if (isset($_SESSION["userDistributionType"]) && $_SESSION["userDistributionType"]=="YES"){
							echo 'checked';
						}else if (!isset($_SESSION["userDistributionType"])){echo 'checked';}
						echo '> 
						&emsp;Yes, we currently have a distribution system in place</h2>';
						echo'<div style="clear:both;"></div><h2 style="float:left;margin-top:20px;font-size:1.2em;"><input type="radio" name="distributionType" 
						value="FUNDS" style="width:2em;height:2em;" ';
						if (isset($_SESSION["userDistributionType"]) && $_SESSION["userDistributionType"]=="FUNDS"){
							echo 'checked';
						}
						echo '>  
						&emsp;No, we do not currently have a distribution system in place, but we have the funding to create one</h2>';
						echo'<div style="clear:both;"></div><h2 style="float:left;margin-top:20px;font-size:1.2em;"><input type="radio" name="distributionType" 
						value="NO" style="width:2em;height:2em;" ';
						if (isset($_SESSION["userDistributionType"]) && $_SESSION["userDistributionType"]=="NO"){
							echo 'checked';
						}
						echo '>
						&emsp;No, we do not currently have a distribution system in place, and we have no plans to create one</h2>
						</div>';

						arrowecho($page);
						echo '</form>';

						if (isset($_POST["waterType"])){
							$_SESSION["userWaterSourceType"] = $_POST["waterType"];
						}

					}

					else if ($page=="11"){
						arrowecho($page);
					}
					
					?>
<?php
include 'footer.php';
?>
